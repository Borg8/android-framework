import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

public final class Tools
{
    public static final void resizeImages(String path_, int width_, int height_)
    {
        try
        {
            String[] files = StorageManager.getFile(path_).list();
            for (int i = 0; i < files.length; ++i)
            {
                Bitmap bitmap = BitmapFactory.decodeFile(path + files[i]);
                bitmap = Bitmap.createScaledBitmap(bitmap, width_, height_, false);
                OutputStream stream = StorageManager.getFileOutputStream(path + i, false);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                stream.close();
            }
        }
        catch (Exception e)
        {
            Logging.logging(e);
        }

    }

	public static final String printDatabase(Cursor cursor_)
	{
		StringBuilder builder = new StringBuilder();

		String[] columns = cursor_.getColumnNames();

		cursor_.moveToFirst();
		int n = cursor_.getColumnCount();
		for (int i = 1; i <= 500; ++i)
		{
			// add row number
			builder.append("------  ");
			builder.append(i);
			builder.append("  ------\n");

			// append row
			for (int j = 0; j < n; ++j)
			{
				builder.append(String.format("%1$-32s", columns[j]));
				builder.append(" = ");
				builder.append(cursor_.getString(j));
				builder.append("\n");
			}

			builder.append("\n");

			// move to next
			if (cursor_.moveToNext() == false)
			{
				break;
			}
		}

		cursor_.close();

		return builder.toString();
	}

	public void startSystemActivities()
	{
		// application details
		Intent intent = new Intent(
			"android.settings.APPLICATION_DETAILS_SETTINGS dat=package:com.example.statistics");
		intent.setFlags(0x10808000);
		intent.setData(Uri.parse("package:com.example.statistics"));
		intent.setComponent(new ComponentName("com.android.settings",
			"com.android.settings.applications.InstalledAppDetailsTop"));
		startActivity(intent);
	}
}
