package borg.framework.holders;

import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.location.Address;
import android.os.Bundle;

import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.Contract;

import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;

@SuppressLint("StaticFieldLeak")
public final class GlobalsHolder
{
	public static final class Message<T>
	{
		/** message tag **/
		@NonNull
		public final String tag;

		/** message object **/
		@Nullable
		public final T object;

		private Message(@NonNull String tag_, @Nullable T object_)
		{
			tag = tag_;
			object = object_;
		}
	}

	private static final class LifecycleCallbacks implements Application.ActivityLifecycleCallbacks
	{
		@Override
		public void onActivityCreated(@NonNull Activity activity, Bundle bundle)
		{
			created.add(activity);
		}

		@Override
		public void onActivityStarted(@NonNull Activity activity_)
		{
			started.add(activity_);

			if (started.size() == 1)
			{
				applicationStateChanged.invoke(true);
			}
		}

		@Override
		public void onActivityResumed(@NonNull Activity activity)
		{
			// nothing to do here

		}

		@Override
		public void onActivityPaused(@NonNull Activity activity)
		{
			// nothing to do here
		}

		@Override
		public void onActivityStopped(@NonNull Activity activity_)
		{
			started.remove(activity_);

			if (started.isEmpty() == true)
			{
				applicationStateChanged.invoke(false);
			}
		}

		@Override
		public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle)
		{
			// nothing to do here
		}

		@Override
		public void onActivityDestroyed(@NonNull Activity activity_)
		{
			created.remove(activity_);
		}
	}

	/** system reboot event **/
	public static final Event<Object> reboot = new Event<>();

	/** Firebase message received event: argument - received message **/
	public static final Event<RemoteMessage> messageReceived = new Event<>();

	/**
	 * application foreground / background state changed: argument - application state, {@code true}
	 * if the application is foreground, {@code false} otherwise
	 **/
	public static final Event<Boolean> applicationStateChanged = new Event<>();

	/** global message broadcast event: argument - broad casted message **/
	public static final Event<Message<?>> globalBroadcast = new Event<>();

	private static boolean sIsFirstExecution = false;

	private static Application sApplication = null;

	private static Context sContext = null;

	private static final LifecycleCallbacks sLifecycleCallbacks = new LifecycleCallbacks();

	private static Resources sResources = null;

	private static AssetManager sAssets = null;

	private static Service sService = null;

	private static AccessibilityService sConnectedService = null;

	private static RemoteMessage sLastMessage = null;

	private static Address sAddress = null;

	private static Integer sTimezone = null;

	private static final Set<Activity> created = new HashSet<>();

	private static final Set<Activity> started = new HashSet<>();

	private GlobalsHolder()
	{
		// private constructor to prevent instantiation
	}

	@NonNull
	@Contract(pure = true)
	public static Application getApplication()
	{
		return sApplication;
	}

	@NonNull
	@Contract(pure = true)
	public static Context getContext()
	{
		return sContext;
	}

	@Nullable
	@Contract(pure = true)
	public static Service getService()
	{
		return sService;
	}

	@NonNull
	@Contract(pure = true)
	public static Resources getResources()
	{
		return sResources;
	}

	@NonNull
	@Contract(pure = true)
	public static AssetManager getAssetManager()
	{
		return sAssets;
	}

	@Nullable
	@Contract(pure = true)
	public static AccessibilityService getConnectedService()
	{
		return sConnectedService;
	}

	@Nullable
	@Contract(pure = true)
	public static Address getAddress()
	{
		return sAddress;
	}

	@Contract(pure = true)
	@Nullable
	public static Integer getTimezone()
	{
		return sTimezone;
	}

	@Contract(pure = true)
	public static boolean isFirstExecution()
	{
		return sIsFirstExecution;
	}

	@Contract(pure = true)
	@NonNull
	public static Set<Activity> getCreatedActivities()
	{
		return created;
	}

	@Contract(pure = true)
	@NonNull
	public static Set<Activity> getStartedActivities()
	{
		return started;
	}

	@Contract(pure = true)
	public static RemoteMessage getLastMessage()
	{
		return sLastMessage;
	}

	public static void setApplication(@NonNull Application application_)
	{
		sApplication = application_;
		sContext = application_.getApplicationContext();
		sResources = sContext.getResources();
		sAssets = sResources.getAssets();

		if (sContext.fileList().length == 0)
		{
			sIsFirstExecution = true;

			try
			{
				FileOutputStream stream = sContext.openFileOutput(".....", Context.MODE_PRIVATE);
				stream.write(0);
				stream.flush();
				stream.close();
			}
			catch (Exception e)
			{
				Logger.log(e);
			}
		}
		else
		{
			sIsFirstExecution = false;
		}

		sApplication.registerActivityLifecycleCallbacks(sLifecycleCallbacks);
	}

	public static void setService(@Nullable Service service_)
	{
		sService = service_;
	}

	public static void setConnectedService(@Nullable AccessibilityService service_)
	{
		sConnectedService = service_;
	}

	public static void systemRebooted()
	{
		reboot.invoke(null);
	}

	public static void setMessage(@NonNull RemoteMessage message_)
	{
		sLastMessage = message_;
		messageReceived.invoke(message_);
	}

	public static void setAddress(@NonNull Address address_)
	{
		sAddress = address_;
	}

	public static void setTimezone(int timezone_)
	{
		sTimezone = timezone_;
	}

	public static void broadcastMessage(@NonNull String tag_, @Nullable Object object_)
	{
		globalBroadcast.invoke(new Message<>(tag_, object_));
	}
}
