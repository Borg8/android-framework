package borg.framework.tools;

import android.content.Context;
import android.os.Vibrator;
import androidx.annotation.NonNull;

import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

/**
 * Acquire VIBRATE permission.
 *
 * @author Borg8
 */
public final class VibratorTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** vibrator instance **/
	private final Vibrator mVibrator;

	/** single instance of VibratorTool **/
	private static VibratorTool mInstance = null;

	/** number of created instances **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private VibratorTool()
	{
		Context context = GlobalsHolder.getContext();

		mVibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
	}

	/**
	 * @return instance of VibratorTool
	 */
	@NonNull
	public static VibratorTool getInstance()
	{
		if (mInstance == null)
		{
			// create single instance of VibratorTool
			mInstance = new VibratorTool();
		}

		++sNInstance;
		return mInstance;
	}

	/**
	 * starts vibration for given time
	 *
	 * @param duration_ time to vibrate
	 */
	public void startVibration(long duration_)
	{
		mVibrator.vibrate(duration_);
	}

	/**
	 * starts vibration for given pattern
	 *
	 * @param pattern_ pattern of times for starting and stopping vibration.
	 * @param repeat_  how many time repeat the pattern. If repeat value is -1 then pattern will not been
	 *                 repeated
	 */
	public void startVibration(@NonNull long[] pattern_, int repeat_)
	{
		mVibrator.vibrate(pattern_, repeat_);
	}

	/**
	 * stops vibration
	 */
	public void stopVibration()
	{
		mVibrator.cancel();
	}

	/**
	 * destroy instance and release it's resources
	 *
	 * @param creator_ object that was getting the instance
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the VibratorTool
				super.release(this);
				stopVibration();

				mInstance = null;
			}
		}
	}
}
