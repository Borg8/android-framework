package borg.framework.tools;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import androidx.annotation.NonNull;

import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;
import borg.framework.resources.Constants;

/**
 * Acquire WAKE_LOCK permission
 *
 * @author Borg
 */
public final class PowerTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final class ScreenWatcher
	{
		//////////////////////////////////////////////////////////////////////////////////////////////////
		// Fields
		//////////////////////////////////////////////////////////////////////////////////////////////////

		/**
		 * screen state changed event: argument - {@code true} if the screen is turned on, {@code false}
		 * otherwise
		 **/
		private final Event<Boolean> screenStateChanged;

		/** screen change change broadcast receiver **/
		private final BroadcastReceiver mScreenStateChangeReceiver;

		/** when true prevents invoking of screen on and screen off event **/
		private boolean mSilenceMode;

		/** single instance **/
		private static ScreenWatcher sInstance = null;

		/** number of created instances **/
		private static int sNInstance = 0;

		//////////////////////////////////////////////////////////////////////////////////////////////////
		// Methods
		//////////////////////////////////////////////////////////////////////////////////////////////////

		private ScreenWatcher()
		{
			screenStateChanged = new Event<>();

			mScreenStateChangeReceiver = new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{
					// if screen turned on
					if (Intent.ACTION_SCREEN_ON.equals(intent.getAction()))
					{
						if (mSilenceMode == false)
						{
							screenStateChanged.invoke(true);
						}
						else
						{
							mSilenceMode = false;
						}
					}
					else
					{
						if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction()))
						{
							if (mSilenceMode == false)
							{
								screenStateChanged.invoke(false);
							}
							else
							{
								mSilenceMode = false;
							}
						}
					}
				}
			};

			IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
			filter.addAction(Intent.ACTION_SCREEN_OFF);
			filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
			GlobalsHolder.getContext().registerReceiver(mScreenStateChangeReceiver, filter);

			mSilenceMode = false;
		}

		/**
		 * @return instance of Screen Watcher
		 */
		private static ScreenWatcher getInstance()
		{
			// create single instance of screen locker
			if (sInstance == null)
			{
				sInstance = new ScreenWatcher();
			}

			++sNInstance;
			return sInstance;
		}

		/**
		 * @param mode_ silence mode to set
		 */
		private void setSilenceMode(boolean mode_)
		{
			mSilenceMode = mode_;
		}

		/**
		 * destroy screen locker and release its resources
		 *
		 * @param creator_ object that was getting the instance
		 */
		private void release(Object creator_)
		{
			if (BuildConfig.DEBUG && sNInstance == 0)
			{
				throw new Error(Messages.exceptionAlreadyReleased());
			}
			else
			{
				screenStateChanged.detach(creator_);

				--sNInstance;
				if (sNInstance == 0)
				{
					GlobalsHolder.getContext().unregisterReceiver(mScreenStateChangeReceiver);

					sInstance = null;
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** screen state changed event: argument - {@code true} if the screen is turned ON, {@code false} otherwise **/
	public final Event<Boolean> screenStateChanged;

	/** single Screen Watcher instance **/
	private final ScreenWatcher mScreenWatcher;

	/** power manager **/
	private final PowerManager mPowerManager;

	/** CPU wake lock **/
	private final WakeLock mCPUWakeLock;

	/** screen wake lock **/
	private final WakeLock mScreenWakeLock;

	/** number of created instances **/
	private static int nInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * constructor
	 */
	@SuppressWarnings("deprecation")
	private PowerTool()
	{
		screenStateChanged = new Event<>();

		++nInstance;

		// create single screen locker instance
		mScreenWatcher = ScreenWatcher.getInstance();
		mScreenWatcher.screenStateChanged.attach(new Event.Observer<Boolean>(this)
		{
			@Override
			public void action(@NonNull Boolean state_)
			{
				screenStateChanged.invoke(state_);
			}
		});

		Context context = GlobalsHolder.getContext();
		mPowerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		String tag = GlobalsHolder.getContext() + ".powerTool.CPU" + nInstance;
		assert mPowerManager != null;
		mCPUWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, tag);
		tag = Constants.NAME_PACKAGE + ".powerTool.screen" + nInstance;
		mScreenWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
			PowerManager.ACQUIRE_CAUSES_WAKEUP, tag);
	}

	/**
	 * @return instance of Power Tool
	 */
	@NonNull
	public static PowerTool getInstance()
	{
		return new PowerTool();
	}

	/**
	 * @return true if screen is on, false otherwise
	 */
	@SuppressWarnings("deprecation")
	public boolean isScreenOn()
	{
		return mPowerManager.isScreenOn();
	}

	/**
	 * @return is CPU locked by the instance of power tool
	 */
	public boolean isCpuLocked()
	{
		return mCPUWakeLock.isHeld();
	}

	/**
	 * @return is screen locked by the instance of power tool
	 */
	public boolean isScreenLocked()
	{
		return mScreenWakeLock.isHeld();
	}

	/**
	 * @param mode_ silence mode to set
	 */
	public void setSilenceMode(boolean mode_)
	{
		mScreenWatcher.setSilenceMode(mode_);
	}

	/**
	 * lock CPU from from sleeping
	 */
	public void lockCpu()
	{
		lockCpu(0);
	}

	/**
	 * lock CPU for given number of milliseconds
	 *
	 * @param timeOut_ number of milliseconds when the lock will be released
	 */
	@SuppressLint("WakelockTimeout")
	public void lockCpu(long timeOut_)
	{
		unlockCpu();

		if (timeOut_ <= 0)
		{
			mCPUWakeLock.acquire();
		}
		else
		{
			mCPUWakeLock.acquire(timeOut_);
		}
	}

	/**
	 * lock screen form turn off
	 */
	public void lockScreen()
	{
		lockScreen(0);
	}

	/**
	 * lock screen for given number of milliseconds
	 *
	 * @param timeOut_ number of milliseconds when the lock will be released
	 */
	@SuppressLint("WakelockTimeout")
	public void lockScreen(long timeOut_)
	{
		unlockScreen();

		if (timeOut_ <= 0)
		{
			mScreenWakeLock.acquire();
		}
		else
		{
			mScreenWakeLock.acquire(timeOut_);
		}
	}

	/**
	 * unlock CPU
	 */
	public void unlockCpu()
	{
		if (isCpuLocked() == true)
		{
			mCPUWakeLock.release();
		}
	}

	/**
	 * unlock screen
	 */
	public void unlockScreen()
	{
		if (isScreenLocked() == true)
		{
			mScreenWakeLock.release();
		}
	}

	/**
	 * destroy instance and release it's resources
	 *
	 * @param creator_ object that was getting the instance
	 */
	public void release(Object creator_)
	{
		super.release(this);

		unlockCpu();
		unlockScreen();

		mScreenWatcher.release(this);
	}
}
