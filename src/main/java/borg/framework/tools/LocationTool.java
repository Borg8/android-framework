package borg.framework.tools;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.util.Xml;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.xmlpull.v1.XmlPullParser;

import java.io.ByteArrayInputStream;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;
import borg.framework.resources.structures.HttpResponse;
import borg.framework.resources.structures.NetworkResult;
import borg.framework.serializers.BinarySerialized;
import borg.framework.services.HttpClient;
import borg.framework.services.ThreadsManager;
import borg.framework.services.TimeManager;

/**
 * Acquire ACCESS_FINE_LOCATION permission
 *
 * @author Borg8
 */
public final class LocationTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final String PATTERN_URL_TIMEZONE_API = "https://maps.googleapis.com/maps/api/timezone/xml?location={0},{1}&timestamp={2}";

	private static final String TAG_TIME_ZONE_RESPONSE = "TimeZoneResponse";

	private static final String TAG_RAW_OFFSET = "raw_offset";

	private static final String TAG_DST_OFFSET = "dst_offset";

	private static final long REQUEST_INTERVAL = 10000;

	private static final long TIMEZONE_EXPIRATION = TimeManager.HOUR * 3;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final class State extends BinarySerialized
	{
		/** last received timezone **/
		int timeZone;

		/** time when the timezone received at last **/
		long timeZoneTime;

		private State()
		{
			super(State.class.getName());
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** location changed event: argument - current location **/
	public final Event<Location> locationChangeEvent;

	/** network provider state changed event: argument - current provider state **/
	public final Event<Boolean> networkProviderStateChangedEvent;

	/** gps provider state changed event: argument - current provider state **/
	public final Event<Boolean> gpsProviderStateChangedEvent;

	/** instance state **/
	private final State mState;

	/** location manager instance **/
	private final LocationManager mLocationManager;

	/** location client instance **/
	private final FusedLocationProviderClient mLocationClient;

	/** current location **/
	@Nullable
	private Location mCurrentLocation;

	/** sign whether location listener are enabled **/
	private boolean mIsLocationListened;

	/** last state of GPS provider **/
	private boolean mLastGpsState;

	/** last state of network provider **/
	private boolean mLastNetworkState;

	/** single instance of LocationTool **/
	private static LocationTool sInstance = null;

	/** number of created instances **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private LocationTool()
	{
		locationChangeEvent = new Event<>();
		networkProviderStateChangedEvent = new Event<>();
		gpsProviderStateChangedEvent = new Event<>();

		// read state
		mState = new State();
		if (mState.readState() == false)
		{
			mState.timeZone = 0;
			mState.timeZoneTime = 0;
		}

		Context context = GlobalsHolder.getContext();
		mLocationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		mLocationClient = LocationServices.getFusedLocationProviderClient(context);

		// create location provider state change receiver
		IntentFilter filter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
		context.registerReceiver(providerStateReceiver, filter);

		mIsLocationListened = false;
		mLastGpsState = isGpsProviderEnabled();
		mLastNetworkState = isNetworkProviderEnabled();
	}

	/**
	 * @return instance of LocationTool
	 */
	@NonNull
	public static LocationTool getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of LocationTool
			sInstance = new LocationTool();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * @return true if GPS is enable, false otherwise
	 */
	@Contract(pure = true)
	public boolean isGpsProviderEnabled()
	{
		try
		{
			return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		catch (Exception e)
		{
			return false;
		}
	}

	/**
	 * @return true if network provider is enable, false otherwise
	 */
	@Contract(pure = true)
	public boolean isNetworkProviderEnabled()
	{
		try
		{
			return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		}
		catch (Exception e)
		{
			return false;
		}
	}

	private final ContentResolver cResolver = GlobalsHolder.getContext().getContentResolver();

	/**
	 * @return true if mock location provider is enabled, false otherwise.
	 */
	@Deprecated
	@Contract(pure = true)
	public boolean isMockProviderEnabled()
	{
		try
		{
			return Settings.Secure.getInt(cResolver, Settings.Secure.ALLOW_MOCK_LOCATION) == 1;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	/**
	 * @return last known location.
	 */
	@Contract(pure = true)
	@Nullable
	public Location getLastLocation()
	{
		return mCurrentLocation;
	}

	/**
	 * compute location age in milliseconds.
	 *
	 * @param location_ location to compute age of it.
	 *
	 * @return location age.
	 */
	@Contract(pure = true)
	public static long getLocationAge(@NonNull Location location_)
	{
		// get location time
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
		{
			long nanos = SystemClock.elapsedRealtimeNanos() - location_.getElapsedRealtimeNanos();
			return nanos / 1000000L;
		}

		return TimeManager.getRealTime() - location_.getTime();
	}

	/**
	 * start listening for location changes.
	 *
	 * @param priority_ request priority, according to {@link LocationRequest} constants.
	 */
	public void startLocationListening(int priority_)
	{
		// stop previous listener
		stopLocationListening();

		// start request location
		LocationRequest request = new LocationRequest();
		request.setInterval(REQUEST_INTERVAL);
		request.setPriority(priority_);
		mLocationClient.requestLocationUpdates(request, locationListener, null);

		mIsLocationListened = true;
	}

	/**
	 * stop listening for location changes
	 */
	public void stopLocationListening()
	{
		if (mIsLocationListened == true)
		{
			mLocationClient.removeLocationUpdates(locationListener);

			mIsLocationListened = false;
		}
	}

	/**
	 * update current address.
	 */
	public void updateAddress()
	{
		// if location available
		if (mCurrentLocation != null)
		{
			ThreadsManager.startAsyncTask(new ThreadsManager.Task("update address")
			{
				@Override
				protected void action()
				{
					synchronized (LocationTool.this)
					{
						try
						{
							// update address
							updateAddress(mCurrentLocation);

							// update timezone
							updateTimezone(mCurrentLocation);
						}
						catch (Exception e)
						{
							Logger.log(e);
						}
					}
				}
			});
		}
		else
		{
			Task<Location> task = mLocationClient.getLastLocation();
			task.addOnSuccessListener(location -> updateAddress());
		}
	}

	private static void updateAddress(@NonNull Location location_) throws Exception
	{
		// obtain addresses
		Geocoder coder = new Geocoder(GlobalsHolder.getContext(), Locale.US);
		List<Address> addresses;
		addresses = coder.getFromLocation(location_.getLatitude(), location_.getLongitude(), 1);

		// if addresses received
		if ((addresses != null) && (addresses.isEmpty() == false))
		{
			// set current address
			GlobalsHolder.setAddress(addresses.iterator().next());
		}
		else
		{
			Logger.log(Log.WARN, "unknown address: " + addresses);
		}
	}

	@SuppressWarnings("MethodCallInLoopCondition")
	private void updateTimezone(@NonNull Location location_) throws Exception
	{
		// if timezone expired
		long now = TimeManager.getRealTime();
		if (now - mState.timeZoneTime > TIMEZONE_EXPIRATION)
		{
			// request timestamp for the location
			HttpClient poster = HttpClient.getInstance();
			String url = MessageFormat.format(PATTERN_URL_TIMEZONE_API,
				location_.getLatitude(),
				location_.getLongitude(),
				Long.toString(TimeManager.getRealTime() / 1000));
			HttpResponse response = poster.sendRequest(url, "POST", null, null, true);

			// if response received
			if (response.result == NetworkResult.SUCCESS)
			{
				ByteArrayInputStream stream = new ByteArrayInputStream(response.content);

				//noinspection TryFinallyCanBeTryWithResources
				try
				{
					Double raw = null;
					Double dst = null;

					// create parser
					XmlPullParser parser = Xml.newPullParser();
					parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
					parser.setInput(stream, null);

					// get time zone tag
					while (TAG_TIME_ZONE_RESPONSE.equals(parser.getName()) == false)
					{
						parser.nextTag();
					}

					// get raw offset tag
					while ((raw == null) || (dst == null))
					{
						// get next tag
						if (parser.next() == XmlPullParser.END_DOCUMENT)
						{
							break;
						}

						// if raw offset found
						if ((raw == null) && (TAG_RAW_OFFSET.equals(parser.getName())))
						{
							// read offset
							while (parser.getEventType() != XmlPullParser.TEXT)
							{
								if (parser.next() == XmlPullParser.END_DOCUMENT)
								{
									break;
								}
							}
							raw = Double.parseDouble(parser.getText());
						}
						else
						{
							if ((dst == null) && (TAG_DST_OFFSET.equals(parser.getName())))
							{
								// read offset
								while (parser.getEventType() != XmlPullParser.TEXT)
								{
									if (parser.next() == XmlPullParser.END_DOCUMENT)
									{
										break;
									}
								}
								dst = Double.parseDouble(parser.getText());
							}
						}
					}

					// set time zone offset
					if ((raw != null) && (dst != null))
					{
						mState.timeZone = (int)((raw + dst) * 1000);
						mState.timeZoneTime = now;
					}
				}
				finally
				{
					stream.close();
				}
			}
			else
			{
				Logger.log("unable to obtain timezone: " + response.result);
			}
		}

		GlobalsHolder.setTimezone(mState.timeZone);
	}

	private void reportProviderState()
	{
		// if state of GPS provider was changed
		boolean state = isGpsProviderEnabled();
		if (state != mLastGpsState)
		{
			mLastGpsState = state;
			gpsProviderStateChangedEvent.invoke(mLastGpsState);
		}
		else
		{
			// if state of network provider was changed
			state = isNetworkProviderEnabled();
			if (state != mLastNetworkState)
			{
				mLastNetworkState = state;
				networkProviderStateChangedEvent.invoke(mLastNetworkState);
			}
		}
	}

	void updateLocation(@Nullable Location location_)
	{
		mCurrentLocation = location_;

		// if location listener was not stopped
		if (mIsLocationListened == true)
		{
			locationChangeEvent.invoke(mCurrentLocation);
		}
	}

	private final LocationCallback locationListener = new LocationCallback()
	{
		@Override
		public void onLocationResult(@NotNull LocationResult result_)
		{
			updateLocation(result_.getLastLocation());
		}
	};

	private final BroadcastReceiver providerStateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context_, Intent intent_)
		{
			reportProviderState();
		}
	};

	/**
	 * destroy instance and release it's resources
	 *
	 * @param creator_ object that was getting the instance
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);
			locationChangeEvent.detach(creator_);
			networkProviderStateChangedEvent.detach(creator_);
			gpsProviderStateChangedEvent.detach(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the LocationTool
				stopLocationListening();
				Context context = GlobalsHolder.getContext();
				context.unregisterReceiver(providerStateReceiver);

				super.release(this);
				sInstance = null;
			}
		}
	}
}
