package borg.framework.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import org.jetbrains.annotations.Contract;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

/**
 * Acquire ACCESS_WIFI_STATE and CHANGE_WIFI_STATE permission
 *
 * @author Borg
 */
public final class WifiTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** capabilities of closed WiFi networks as regex **/
	public static final String CAPABILITIES_CLOSED_NETWORK = ".*WPA.*|.*WEP.*|.*WPS.*";

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * WiFi adapter state changed event: argument - current adapter state, according to
	 * {@link WifiManager} constants
	 **/
	public final Event<Integer> adapterStateChanged;

	/** scanning for WiFi networks complete event: arguments - scanning result list **/
	public final Event<List<ScanResult>> scanCompleted;

	/** WiFi manager instance **/
	private final WifiManager mWifiManager;

	/** scan results of last WiFi network scanning **/
	private List<ScanResult> mScanResults;

	/** is broadcast to wifi scanning registered **/
	private boolean mIsRegistered;

	/** single instance of WifiTool. **/
	private static WifiTool sInstance = null;

	/** number of created instances. **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private WifiTool()
	{
		adapterStateChanged = new Event<>();
		scanCompleted = new Event<>();

		Context context = GlobalsHolder.getContext();
		mWifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
		filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
		context.registerReceiver(stateChangeReceived, filter);

		mIsRegistered = false;
	}

	/**
	 * @return instance of WifiTool.
	 */
	@NonNull
	public static WifiTool getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of WifiTool
			sInstance = new WifiTool();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * @return WiFi manager.
	 */
	@NonNull
	@Contract(pure = true)
	public WifiManager getManager()
	{
		return mWifiManager;
	}

	/**
	 * @return {@code true} if WiFi enabled, {@code false} otherwise.
	 */
	@Contract(pure = true)
	public boolean isEnabled()
	{
		return mWifiManager.isWifiEnabled();
	}

	/**
	 * @return {@code true} if WiFi connected to network, {@code false} otherwise.
	 */
	@Contract(pure = true)
	public boolean isConnected()
	{
		WifiInfo network = getConnectedWifiNetwork();
		return (network != null) && (network.getNetworkId() >= 0);
	}

	/**
	 * @return connected WiFi network , or {@code null} if such network is not exist.
	 */
	@Nullable
	@Contract(pure = true)
	public WifiInfo getConnectedWifiNetwork()
	{
		if (isEnabled() == true)
		{
			return mWifiManager.getConnectionInfo();
		}

		return null;
	}

	/**
	 * @return list of configured WiFi networks.
	 */
	@Contract(pure = true)
	public List<WifiConfiguration> getConfiguredNetworks()
	{
		return mWifiManager.getConfiguredNetworks();
	}

	/**
	 * retrieve ID of WiFi network by it's name. If more than one network with given name are exists first one
	 * will be retrieved.
	 *
	 * @param networkName_ name of WiFi network.
	 *
	 * @return networks id, or negative number if network with given name not exists.
	 */
	@Contract(pure = true)
	public int getNetworkId(@NonNull String networkName_)
	{
		int id = -1;
		networkName_ = wrapStringWithQuotes(networkName_);
		List<WifiConfiguration> networks = mWifiManager.getConfiguredNetworks();

		for (WifiConfiguration network : networks)
		{
			if (network.SSID.equals(networkName_))
			{
				id = network.networkId;
			}
		}

		return id;
	}

	/**
	 * @return list of WiFi networks that was found at last scan. Return null if no network were found.
	 */
	@Nullable
	@Contract(pure = true)
	public List<ScanResult> getLastScanResults()
	{
		return mScanResults;
	}

	/**
	 * enable WiFi adapter.
	 *
	 * @return operation result.
	 */
	public boolean enableAdapter()
	{
		return mWifiManager.setWifiEnabled(true);
	}

	/**
	 * disable WiFi adapter.
	 *
	 * @return operation result.
	 */
	public boolean disableAdapter()
	{
		return mWifiManager.setWifiEnabled(false);
	}

	/**
	 * create open network configuration. If network already exists then do nothing.
	 *
	 * @param name_ name of added network.
	 *
	 * @return id of created network, or negative number if some trouble has been happened.
	 */
	@Contract(pure = true)
	public int createWifiNetwork(@NonNull String name_)
	{
		// get network id
		int id = getNetworkId(name_);

		// if network not exists
		if (id < 0)
		{
			WifiConfiguration configuration = new WifiConfiguration();

			// configure the network
			configuration.SSID = wrapStringWithQuotes(name_);
			configuration.hiddenSSID = true;
			configuration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

			// add network
			id = mWifiManager.addNetwork(configuration);
		}

		return id;
	}

	/**
	 * remove network from known WiFi network lists.
	 *
	 * @param id_ id of network to remove.
	 */
	public void removeWifiNetwork(int id_)
	{
		if (id_ >= 0)
		{
			mWifiManager.removeNetwork(id_);
		}
	}

	/**
	 * start connect to network by its id.
	 *
	 * @param id_ id of network to connect.
	 *
	 * @return {@code true} if action succeeded, {@code false} otherwise.
	 */
	public boolean connectToWifiNetwork(int id_)
	{
		return mWifiManager.enableNetwork(id_, true);
	}

	/**
	 * disconnect from WiFi network.
	 *
	 * @return true if operation is succeed, false otherwise.
	 */
	public boolean disconnectFromWifiNetwork()
	{
		return mWifiManager.disconnect();
	}

	/**
	 * start to scan for available WiFi networks.
	 */
	public boolean startScan()
	{
		// if receiver not registered
		if (mIsRegistered == false)
		{
			// register broadcast receivers
			Context context = GlobalsHolder.getContext();
			IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
			context.registerReceiver(scanResultReceiver, filter);

			mIsRegistered = true;
		}

		return mWifiManager.startScan();
	}

	@Contract(pure = true)
	@NonNull
	private static String wrapStringWithQuotes(@NonNull String string_)
	{
		if (string_.isEmpty() == false)
		{
			if (string_.charAt(0) != '\"')
			{
				string_ = "\"" + string_;
			}
			if (string_.charAt(string_.length() - 1) != '\"')
			{
				string_ += "\"";
			}
		}
		else
		{
			string_ = "\"\"";
		}

		return string_;
	}

	private final BroadcastReceiver stateChangeReceived = new BroadcastReceiver()
	{
		public void onReceive(Context context_, Intent intent_)
		{
			int state = intent_.getIntExtra(WifiManager.EXTRA_WIFI_STATE, Integer.MIN_VALUE);

			if ((state == WifiManager.WIFI_STATE_DISABLED) || (state == WifiManager.WIFI_STATE_ENABLED))
			{
				adapterStateChanged.invoke(state);
			}
		}
	};

	private final BroadcastReceiver scanResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context_, Intent intent_)
		{
			// if wifi is enabled
			if (isEnabled() == true)
			{
				mScanResults = mWifiManager.getScanResults();

				scanCompleted.invoke(mScanResults);
			}
		}
	};

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			adapterStateChanged.detach(creator_);
			scanCompleted.detach(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the WifiTool
				Context context = GlobalsHolder.getContext();
				context.unregisterReceiver(stateChangeReceived);
				context.unregisterReceiver(scanResultReceiver);

				super.release(this);
				sInstance = null;
			}
		}
	}
}
