package borg.framework.tools;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;

import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

/**
 * @author Borg
 */
public final class SensorsTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** sensor manager instance **/
	private final SensorManager mSensorManager;

	/** sensor instance **/
	private final Sensor mSensor;

	/** sensor events listener **/
	private SensorEventListener mSensorListener;

	/** handler thread to handle sensor events in other thread **/
	private HandlerThread mHandlerThread;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * constructor
	 */
	private SensorsTool(int type_)
	{
		Context context = GlobalsHolder.getContext();
		mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		if (mSensorManager == null)
		{
			throw new Error(Messages.exceptionInvalidArgument());
		}

		// creating sensor
		mSensor = mSensorManager.getDefaultSensor(type_);

		mSensorListener = null;
	}

	/**
	 * Accelerometer Tools builder
	 *
	 * @param type_ sensor type. According to Sensor constants.
	 *
	 * @return new instance of Accelerometer Tools
	 */
	public static SensorsTool createInstance(int type_)
	{
		return new SensorsTool(type_);
	}

	/**
	 * @return true if tool is listening for sensor changed, false otherwise.
	 */
	public boolean isStarted()
	{
		return mSensorListener != null;
	}

	/**
	 * starts listening for sensor in fastest sensor rate.
	 *
	 * @param listener_ sensor events listener to receive sensor events.
	 */
	public void startListening(@NonNull SensorEventListener listener_)
	{
		startListening(listener_, SensorManager.SENSOR_DELAY_FASTEST);
	}

	/**
	 * starts listening for sensor.
	 *
	 * @param listener_ sensor events listener to receive sensor events.
	 * @param delay_    - delay between sensors probes according to {@link SensorManager} constants.
	 */
	public void startListening(@NonNull SensorEventListener listener_, int delay_)
	{
		if (mSensorListener == null)
		{
			mSensorListener = listener_;

			// create handler
			mHandlerThread = new HandlerThread("sensor: " + mSensor.getName());
			mHandlerThread.start();
			Handler handler = new Handler(mHandlerThread.getLooper());

			mSensorManager.registerListener(mSensorListener, mSensor, delay_, handler);
		}
	}

	/**
	 * stops listening for sensor.
	 */
	public void stopListening()
	{
		if (mSensorListener != null)
		{
			mHandlerThread.quit();
			mSensorManager.unregisterListener(mSensorListener);
			mSensorListener = null;
		}
	}

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		stopListening();

		super.release(this);
	}
}
