package borg.framework.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.AudioManager;
import android.util.Log;

import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

@SuppressWarnings("deprecation")
public final class CameraTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** picture was taken event: argument - taken picture **/
	public final Event<Bitmap> pictureTaken;

	/** audio manager instance **/
	private final AudioManager mAudioManager;

	/** camera instance **/
	private Camera mCamera;

	/** array represents last taken picture **/
	private byte[] mLastPictureData;

	/** bitmap of last taken photo **/
	private Bitmap mLastBitmap;

	/** single instance of CameraTool. **/
	private static CameraTool mInstance = null;

	/** number of created instances. **/
	private static int mNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private CameraTool()
	{
		pictureTaken = new Event<>();

		Context context = GlobalsHolder.getContext();
		mAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

		mCamera = null;
	}

	/**
	 * @return instance of CameraTool.
	 */
	public static CameraTool getInstance()
	{
		if (mInstance == null)
		{
			// create single instance of CameraTool
			mInstance = new CameraTool();
		}

		++mNInstance;
		return mInstance;
	}

	/**
	 * @return number of available cameras in the phone.
	 */
	public int getNumCameras()
	{
		return Camera.getNumberOfCameras();
	}

	/**
	 * @return array represents last taken picture.
	 */
	public byte[] getTakenPictureData()
	{
		return mLastPictureData;
	}

	/**
	 * @return last taken picture.
	 */
	public Bitmap getTakenPicture()
	{
		return mLastBitmap;
	}

	/**
	 * take photo.
	 *
	 * @param camera_ id of camera to use according to {@link Camera.CameraInfo} constants.
	 * @param flash_  flash state according to {@link Camera.Parameters} constants.
	 * @param sound_  if true then camera sound will be played, if false then not.
	 *
	 * @return true if picture was taken successfully, false otherwise.
	 */
	public boolean takePicture(int camera_, String flash_, final boolean sound_)
	{
		if (mCamera == null)
		{
			final int ringerMode;
			if (sound_ == false)
			{
				// silence all sounds
				ringerMode = mAudioManager.getRingerMode();
				mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
			}
			else
			{
				ringerMode = -1;
			}

			try
			{
				// start preview
				mCamera = Camera.open(camera_);
				mCamera.startPreview();

				// set camera parameters
				Parameters params = mCamera.getParameters();
				params.setFlashMode(flash_);
				params.setJpegQuality(100);
				params.setPictureFormat(ImageFormat.JPEG);
				params.setZoom(0);

				// take picture
				//noinspection AnonymousClassVariableHidesContainingMethodVariable
				mCamera.takePicture(null, null, null, (data_, camera_1) ->
				{
					// release camera
					mCamera.release();
					mCamera = null;

					// store taken picture
					mLastPictureData = data_;
					mLastBitmap = BitmapFactory.decodeByteArray(mLastPictureData,
						0,
						mLastPictureData.length);

					// if ringer was silenced
					if (sound_ == false)
					{
						// rollback ringer
						mAudioManager.setRingerMode(ringerMode);
					}

					// invoke event
					pictureTaken.invoke(mLastBitmap);
				});

				return true;
			}
			catch (Exception e)
			{
				Logger.log(e);

				// if ringer was silenced
				if (sound_ == false)
				{
					// rollback ringer
					mAudioManager.setRingerMode(ringerMode);
				}

				if (mCamera != null)
				{
					mCamera.release();
					mCamera = null;
				}
			}
		}

		return false;
	}

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		if (mNInstance == 0)
		{
			Logger.log(Log.ERROR, new Exception(Messages.exceptionAlreadyReleased()));
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			pictureTaken.detach(creator_);

			--mNInstance;
			if (mNInstance == 0)
			{
				// release resources of the CameraTool
				super.release(this);

				mInstance = null;
			}
		}
	}
}
