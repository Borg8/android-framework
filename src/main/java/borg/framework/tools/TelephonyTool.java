package borg.framework.tools;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

import androidx.annotation.NonNull;
import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;
import borg.framework.resources.Constants;

@SuppressLint("MissingPermission")
public final class TelephonyTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public static final class PhoneNumber implements Serializable
	{
		private static final long serialVersionUID = Constants.VERSION;

		public static final PhoneNumber PRIVATE = new PhoneNumber("");

		private static final int LENGTH_GENERAL_NUMBER = 8;

		/** raw phone number **/
		public final String number;

		private final String mNormalizedNumber;

		public PhoneNumber(@Nullable String number_)
		{
			if (number_ == null)
			{
				number_ = PRIVATE.number;
			}

			number = number_;

			mNormalizedNumber = normalize(number_);
		}

		@Contract("null -> false")
		@Override
		public boolean equals(Object object_)
		{
			if (object_ instanceof PhoneNumber)
			{
				return mNormalizedNumber.equals(((PhoneNumber)object_).mNormalizedNumber);
			}

			if (object_ instanceof String)
			{
				return mNormalizedNumber.equals(normalize((String)object_));
			}

			return false;
		}

		@Override
		public int hashCode()
		{
			return mNormalizedNumber.hashCode();
		}

		@Contract(pure = true)
		@Override
		@NonNull
		public String toString()
		{
			return number;
		}

		@Contract("null -> null")
		private static String normalize(@Nullable String number_)
		{
			if (number_ != null)
			{
				// remove all non digits symbols
				//noinspection ResultOfMethodCallIgnored
				number_.replaceAll("[^0-9.]", "");

				// if number is general
				int len = number_.length();
				if (len > LENGTH_GENERAL_NUMBER)
				{
					number_ = number_.substring(len - LENGTH_GENERAL_NUMBER);
				}
			}

			return number_;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** phone state was changed event: arguments current phone state according to {@link TelephonyManager} constants **/
	public final Event<Integer> phoneStateChanged;

	/** telephony manager **/
	private final TelephonyManager mTelephonyManager;

	/** previous cell ID **/
	private int mLastCellId;

	/** previous cell location **/
	private int mLastLac;

	/** indicate is outgoing call was made at last **/
	private boolean mIsOutgoingCallMade;

	/** last incoming call number **/
	private PhoneNumber mLastIncomingNumber;

	/** last outgoing call number **/
	private PhoneNumber mLastOutgoingNumber;

	/** made outgoing call number **/
	private PhoneNumber mMadeOutgoingNumber;

	/** single instance of TelephonyTool. **/
	private static TelephonyTool sInstance = null;

	/** number of created instances. **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private TelephonyTool()
	{
		phoneStateChanged = new Event<>();

		Context context = GlobalsHolder.getContext();
		mTelephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

		// register broadcast receiver for phone state changes
		IntentFilter filter = new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
		filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
		context.registerReceiver(phoneStateReceiver, filter);

		// register broadcast receiver for outgoing calls
		filter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
		filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
		context.registerReceiver(outgoingCallsReceiver, filter);
	}

	/**
	 * @return single instance of TelephonyTool.
	 */
	@NonNull
	public static TelephonyTool getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of TelephonyTool
			sInstance = new TelephonyTool();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * @return last incoming number
	 */
	@Contract(pure = true)
	public PhoneNumber getLastIncoming()
	{
		return mLastIncomingNumber;
	}

	/**
	 * @return last accepted outgoing number
	 */
	@Contract(pure = true)
	public PhoneNumber getLastOutgoing()
	{
		return mLastOutgoingNumber;
	}

	/**
	 * @return phone state according to {@link TelephonyManager} class constants.
	 */
	@Contract(pure = true)
	public int getPhoneState()
	{
		return mTelephonyManager.getCallState();
	}

	/**
	 * @return phone operator.
	 */
	@Contract(pure = true)
	public String getOperator()
	{
		return mTelephonyManager.getNetworkOperator();
	}

	/**
	 * @return phone operator name.
	 */
	@Contract(pure = true)
	public String getOperatorName()
	{
		return mTelephonyManager.getSimOperatorName();
	}

	/**
	 * @return cell antenna id. -1 will be return if id was unreachable
	 */
	@Contract(pure = true)
	public int getCellId()
	{
		updateCellInfo();

		return mLastCellId;
	}

	/**
	 * @return location id.
	 */
	@Contract(pure = true)
	public int getLac()
	{
		updateCellInfo();

		return mLastLac;
	}

	/**
	 * make a call number.
	 * @param number_ number to call.
	 */
	public static void makeCall(@NonNull PhoneNumber number_)
	{
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setData(Uri.parse("tel:" + number_));
		Context context = GlobalsHolder.getContext();
		context.startActivity(intent);
	}

	private void updateCellInfo()
	{
		// get current cell location
		GsmCellLocation cellLocation = (GsmCellLocation)mTelephonyManager.getCellLocation();

		// if cell location was read properly
		if (cellLocation != null)
		{
			// get current cell id
			int cid = cellLocation.getCid();
			int lac = cellLocation.getLac();

			// if id was not read
			if (cid > 0)
			{
				mLastCellId = cid & 0xffff;
				mLastLac = lac;
			}
		}
		else
		{
			mLastCellId = -1;
			mLastLac = -1;
		}
	}

	private final BroadcastReceiver phoneStateReceiver = new BroadcastReceiver()
	{
		private int prevState = TelephonyManager.CALL_STATE_IDLE;

		@Override
		public void onReceive(Context context_, Intent intent_)
		{
			int state = prevState;
			try
			{
				state = mTelephonyManager.getCallState();

				// if call was not ended
				if (state != TelephonyManager.CALL_STATE_IDLE)
				{
					// if outgoing call was made
					if (mIsOutgoingCallMade == true)
					{
						mIsOutgoingCallMade = false;

						mLastOutgoingNumber = mMadeOutgoingNumber;
					}
					// else incoming call was made
					else
					{
						String number = intent_.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
						mLastIncomingNumber = new PhoneNumber(number);
					}
				}
			}
			catch (Exception e)
			{
				Logger.log(e);
			}

			// if state was changed
			if (state != prevState)
			{
				phoneStateChanged.invoke(state);

				prevState = state;
			}
		}
	};
	private final BroadcastReceiver outgoingCallsReceiver = new BroadcastReceiver()
	{
		public void onReceive(Context context_, @NotNull Intent intent_)
		{
			// store outgoing call information
			mIsOutgoingCallMade = true;
			String number = intent_.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
			mMadeOutgoingNumber = new PhoneNumber(number);
		}
	};

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			phoneStateChanged.detach(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the TelephonyTool
				Context context = GlobalsHolder.getContext();
				context.unregisterReceiver(phoneStateReceiver);
				context.unregisterReceiver(outgoingCallsReceiver);

				super.release(this);
				sInstance = null;
			}
		}
	}
}
