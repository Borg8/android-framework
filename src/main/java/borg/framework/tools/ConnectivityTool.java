package borg.framework.tools;

import android.net.TrafficStats;
import android.os.Process;

import org.jetbrains.annotations.Contract;

import java.text.MessageFormat;

import androidx.annotation.NonNull;
import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.resources.Constants;
import borg.framework.serializers.BinarySerialized;
import borg.framework.resources.structures.Result;

/**
 * Acquire INTERNET, READ_PHONE_STATE, ACCESS_NETWORK_STATE, ACCESS_WIFI_STATE permission.
 *
 * @author Borg
 */
public final class ConnectivityTool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public enum ResultType
	{
		/** 0: unknown result **/
		UNKNOWN,

		/** 1: operation was succeeded **/
		SUCCESS,

		/** 2: general failure **/
		FAIL,

		/** 3: network is busy **/
		BUSY,

		/** 4: maximum data usage has been reached **/
		TRAFFIC,

		/** 5: connectivity forbidden in roaming **/
		ROAMING,
	}

	private static final class State extends BinarySerialized
	{
		private static final long serialVersionUID = Constants.VERSION;

		/** data usage from last reboot **/
		public long lastTraffic;

		/** whole data usage **/
		public long wholeTraffic;

		public State(@NonNull String tag_)
		{
			super(tag_);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** instance state **/
	private final State mState;

	/** maximum amount of bytes that can be sent and received by the service **/
	private long mMaxPermittedTraffic;

	/** sign whether network is permitted in roaming **/
	private boolean mIsRoamingPermitted;

	/** single instance of Communicator **/
	private static ConnectivityTool sInstance = null;

	/** number of created instances **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * constructor
	 */
	private ConnectivityTool()
	{
		// read state
		mState = new State(getClass().getName());
		if (mState.readState() == false)
		{
			mState.lastTraffic = 0;
			mState.wholeTraffic = 0;
		}

		mMaxPermittedTraffic = -1;
		mIsRoamingPermitted = false;
	}

	/**
	 * retrieve instance of Device Scanner if built, or null if the instance was not built.
	 *
	 * @return instance of Device Scanner.
	 */
	@NonNull
	public static ConnectivityTool getInstance()
	{
		if (sInstance == null)
		{
			sInstance = new ConnectivityTool();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * get expected result for attempt to send data with given tag.
	 *
	 * @return expected result of sending operation.
	 */
	@Contract(pure = true)
	@NonNull
	public Result<ResultType> getExpectedResult()
	{
		// TODO handle roaming
		// TODO perform full test
		if (isTrafficAvailable() == true)
		{
			return new Result<>(this, ResultType.SUCCESS);
		}

		return new Result<>(this, ResultType.TRAFFIC,
			MessageFormat.format("used: {0}, allowed: {1}",
				getTraffic(),
				mMaxPermittedTraffic));
	}

	/**
	 * @return true if maximum data usage was not reached, false otherwise.
	 */
	@Contract(pure = true)
	public boolean isTrafficAvailable()
	{
		// if permitted traffic is defined
		if (mMaxPermittedTraffic >= 0)
		{
			// if traffic available
			return getTraffic() < mMaxPermittedTraffic;
		}

		return true;
	}

	/**
	 * @return {@code true} if roaming is permitted, {@code false} otherwise.
	 */
	@Contract(pure = true)
	public boolean isRoamingPermitted()
	{
		return mIsRoamingPermitted;
	}

	/**
	 * @return amount of data usage in bytes.
	 */
	@Contract(pure = true)
	public long getTraffic()
	{
		updateTraffic();
		return mState.wholeTraffic + mState.lastTraffic;
	}

	/**
	 * set max amount of permitted network traffic.
	 *
	 * @param traffic_ max permitted traffic. -1 to unlimited.
	 */
	public void setPermittedTraffic(long traffic_)
	{
		mMaxPermittedTraffic = traffic_;
	}

	/**
	 * set behavior in roaming.
	 *
	 * @param isPermitted_ if true network is permitted in roaming, if false forbidden.
	 */
	public void setInRoamingBehavior(boolean isPermitted_)
	{
		mIsRoamingPermitted = isPermitted_;
	}

	/**
	 * reset data usage information to 0.
	 */
	public void resetTraffic()
	{
		mState.wholeTraffic = 0;

		mState.saveState();
	}

	private void updateTraffic()
	{
		// if last recorded traffic was reset
		long lastTraffic = getLastTraffic();
		if (lastTraffic < mState.lastTraffic)
		{
			mState.wholeTraffic += mState.lastTraffic;
			mState.lastTraffic = 0;
		}
		else
		{
			mState.lastTraffic = lastTraffic;
		}

		mState.saveState();
	}

	@Contract(pure = true)
	private static long getLastTraffic()
	{
		int uid = Process.myUid();

		// return last recorded traffic for current process
		return TrafficStats.getUidRxBytes(uid) + TrafficStats.getUidTxBytes(uid);
	}

	/**
	 * destroy instance and release it's resources
	 *
	 * @param creator_ object that was getting the instance
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				super.release(this);
				sInstance = null;
			}
		}
	}
}
