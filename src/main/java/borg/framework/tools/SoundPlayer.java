package borg.framework.tools;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

public final class SoundPlayer extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** sound was played event **/
	public final Event<Object> soundPlayed;

	/** audio manager instance **/
	private final AudioManager mAudioManager;

	/** media player playing current sound **/
	private MediaPlayer mMediaPlayer;

	/** single instance of SoundPlayer. **/
	private static SoundPlayer sInstance = null;

	/** number of created instances. **/
	private static int sNInstance = 0;

	/**************************************************
	 * Methods
	 **************************************************/

	private SoundPlayer()
	{
		soundPlayed = new Event<>();

		Context context = GlobalsHolder.getContext();
		mAudioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
	}

	/**
	 * @return single instance of SoundPlayer.
	 */
	@NonNull
	public static SoundPlayer getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of SoundPlayer
			sInstance = new SoundPlayer();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * play sound.
	 *
	 * @param uri_   path to file to play.
	 * @param force_ if {@code true} then sound will be played even if phone is muted.
	 *
	 * @return {@code true} if sound started to play, {@code false} otherwise.
	 */
	public boolean play(@NonNull Uri uri_, boolean force_)
	{
		// stop previous sound
		stop();

		// get current ringer mode
		int ringerMode = mAudioManager.getRingerMode();

		if (force_ == true)
		{
			// set ringer volume
			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
				0);
		}
		else
		{
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				mAudioManager.getStreamVolume(AudioManager.STREAM_RING),
				0);
		}

		// play sound
		Context context = GlobalsHolder.getContext();
		mMediaPlayer = MediaPlayer.create(context, uri_);

		// if media player was created successfully
		if (mMediaPlayer != null)
		{
			try
			{
				// start to play
				mMediaPlayer.start();
				mMediaPlayer.setOnCompletionListener(completionListener);

				return true;
			}
			catch (Exception e)
			{
				stop();
				Logger.log(e);
			}
		}

		// return ringer
		mAudioManager.setRingerMode(ringerMode);

		return false;
	}

	/**
	 * stop playing sound.
	 */
	public void stop()
	{
		if (mMediaPlayer != null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	private final MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener()
	{
		@Override
		public void onCompletion(MediaPlayer player_)
		{
			soundPlayed.invoke(null);
		}
	};

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		if (sNInstance == 0)
		{
			Logger.log(Log.ERROR, new Exception(Messages.exceptionAlreadyReleased()));
		}
		else
		{
			// release resources for given creator
			super.release(creator_);
			soundPlayed.detach(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the SoundPlayer
				stop();

				super.release(this);
				sInstance = null;
			}
		}
	}
}
