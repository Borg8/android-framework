package borg.framework.tools;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession.CaptureCallback;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureRequest.Builder;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Build;
import android.os.SystemClock;
import android.util.Size;
import android.view.Surface;

import org.jetbrains.annotations.Contract;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import borg.framework.BuildConfig;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public final class Camera2Tool extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long DURATION_CAPTURE = 1500L;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** picture was taken event: arguments - taken bitmap **/
	public final Event<Bitmap> pictureTaken;

	/** camera manager instance **/
	private final CameraManager mCameraManager;

	/** texture to focus surface **/
	private final SurfaceTexture mFocusTexture;

	/** current camera device instance **/
	private CameraDevice mCameraDevice;

	/** current capture request **/
	private CaptureRequest mCaptureRequest;

	/** current capture session **/
	private CameraCaptureSession mCaptureSession;

	/** image reader of current capture session **/
	private ImageReader mImageReader;

	/** photo width in pixels **/
	private int mWidth;

	/** photo height in pixels **/
	private int mHeight;

	/** flash mode **/
	private int mFlash;

	/** time when capture process was started **/
	private long mCaptureTime;

	/** array represents last taken picture **/
	private byte[] mLastPictureData;

	/** bitmap of last taken photo **/
	private Bitmap mLastBitmap;

	/** single instance of Camera2Tool. **/
	private static Camera2Tool sInstance = null;

	/** number of created instances. **/
	private static int sNInstance = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private Camera2Tool()
	{
		pictureTaken = new Event<>();

		Context context = GlobalsHolder.getContext();
		mCameraManager = (CameraManager)context.getSystemService(Context.CAMERA_SERVICE);

		mCameraDevice = null;

		mFocusTexture = new SurfaceTexture(0);
		mFocusTexture.setDefaultBufferSize(176, 144);
	}

	/**
	 * @return single instance of Camera2Tool.
	 */
	public static Camera2Tool getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of Camera2Tool
			sInstance = new Camera2Tool();
		}

		++sNInstance;
		return sInstance;
	}

	/**
	 * @return array represents last taken picture or null if any troubles were happened.
	 */
	@Contract(pure = true)
	public byte[] getTakenPictureData()
	{
		return mLastPictureData;
	}

	/**
	 * @return last taken picture or null if any troubles were happened.
	 */
	@Contract(pure = true)
	public Bitmap getTakenPicture()
	{
		return mLastBitmap;
	}

	/**
	 * take photo.
	 *
	 * @param isFront_ if true then front camera will be used, if false then back camera.
	 * @param width_   photo width in pixels.
	 * @param height_  photo height in pixels.
	 * @param flash_   flash mode according to {@link CaptureRequest} constants.
	 *
	 * @return true capture session was started successfully, false otherwise.
	 */
	public boolean takePicture(boolean isFront_, int width_, int height_, int flash_)
	{
		// if camera is ready
		if (mCameraDevice == null)
		{
			try
			{
				// choose camera ID
				int camera;
				if (isFront_ == true)
				{
					camera = CameraCharacteristics.LENS_FACING_FRONT;
				}
				else
				{
					camera = CameraCharacteristics.LENS_FACING_BACK;
				}
				String cameraId = null;
				CameraCharacteristics characteristics = null;
				for (String id : mCameraManager.getCameraIdList())
				{
					characteristics = mCameraManager.getCameraCharacteristics(id);

					if (((Integer)camera).equals(characteristics.get(CameraCharacteristics.LENS_FACING)))
					{
						cameraId = id;
						break;
					}
				}

				// if camera was found
				if (cameraId != null)
				{
					// get camera configuration
					StreamConfigurationMap map;
					map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
					if (map != null)
					{
						// get possible sizes
						Size[] sizes = map.getOutputSizes(ImageFormat.JPEG);

						// get minimum match size
						mWidth = Integer.MAX_VALUE;
						mHeight = Integer.MAX_VALUE;
						for (Size size : sizes)
						{
							int w = size.getWidth();
							int h = size.getHeight();

							// if match size found
							if ((w >= width_) && (h >= height_))
							{
								// if size is smaller
								if ((w <= mWidth) && (h <= mHeight))
								{
									mWidth = w;
									mHeight = h;
								}
								else
								{
									// compute difference
									int dw = width_ - w;
									int dh = height_ - h;
									int difference = dw * dw + dh * dh;

									// if smaller size was found
									dw = width_ - mWidth;
									dh = height_ - mHeight;
									if (difference < (dw * dw + dh * dh))
									{
										mWidth = w;
										mHeight = h;
									}
								}
							}
						}

						mFlash = flash_;
						mCameraManager.openCamera(cameraId, cameraCallback, null);

						return true;
					}
				}
			}
			catch (Exception e)
			{
				Logger.log(e);
			}
		}

		return false;
	}

	private void closeSession()
	{
		// close image reader
		if (mImageReader != null)
		{
			mImageReader.close();
			mImageReader = null;
		}

		// close session
		if (mCaptureSession != null)
		{
			try
			{
				mCaptureSession.stopRepeating();
			}
			catch (Exception e_)
			{
				Logger.log(e_);
			}
			mCaptureSession.close();
			mCaptureSession = null;
		}

		// close camera
		if (mCameraDevice != null)
		{
			mCameraDevice.close();
			mCameraDevice = null;
		}
	}

	private void invokeObservers(byte[] pictureData_)
	{
		// close capture session
		closeSession();

		mLastPictureData = pictureData_;

		// if picture data is available
		if (mLastPictureData != null)
		{
			// create bitmap
			mLastBitmap = BitmapFactory.decodeByteArray(mLastPictureData,
				0,
				mLastPictureData.length);
		}
		else
		{
			mLastBitmap = null;
		}

		pictureTaken.invoke(mLastBitmap);

		// clear references to free memory
		mLastBitmap = null;
		mLastPictureData = null;
	}

	private final CameraDevice.StateCallback cameraCallback = new CameraDevice.StateCallback()
	{
		@Override
		public void onOpened(@NonNull CameraDevice camera_)
		{
			try
			{
				mCameraDevice = camera_;

				// create image reader
				mImageReader = ImageReader.newInstance(mWidth, mHeight, ImageFormat.JPEG, 1);
				mImageReader.setOnImageAvailableListener(imageListener, null);

				// create list of surface
				ArrayList<Surface> list = new ArrayList<>(2);
				Surface surface = mImageReader.getSurface();
				Surface focusSurface = new Surface(mFocusTexture);
				list.add(focusSurface);
				list.add(surface);

				// create capture request for still capture
				Builder builder = camera_.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
				builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);

				// add flash property
				builder.set(CaptureRequest.CONTROL_AE_MODE, mFlash);

				// add surface
				builder.addTarget(surface);

				// create capture request
				mCaptureRequest = builder.build();

				// create capture session
				camera_.createCaptureSession(list, captureCallback, null);
			}
			catch (Exception e)
			{
				Logger.log(e);

				// invoke observers
				invokeObservers(null);
			}
		}

		@Override
		public void onError(@NonNull CameraDevice camera_, int error_)
		{
			invokeObservers(null);
		}

		@Override
		public void onDisconnected(@NonNull CameraDevice camera_)
		{
			closeSession();
		}
	};

	private final CameraCaptureSession.StateCallback captureCallback = new CameraCaptureSession.StateCallback()
	{
		@Override
		public void onConfigured(@NonNull CameraCaptureSession session_)
		{
			mCaptureSession = session_;

			try
			{
				// start capture process
				mCaptureSession.setRepeatingRequest(mCaptureRequest, captureListener, null);
				mCaptureTime = SystemClock.elapsedRealtime();
			}
			catch (Exception e)
			{
				Logger.log(e);

				invokeObservers(null);
			}
		}

		@Override
		public void onConfigureFailed(@NonNull CameraCaptureSession session_)
		{
			invokeObservers(null);
		}
	};

	private final CaptureCallback captureListener = new CaptureCallback()
	{
		@Override
		public void onCaptureFailed(@NonNull CameraCaptureSession session_,
			@NonNull CaptureRequest request_,
			@NonNull CaptureFailure failure_)
		{
			invokeObservers(null);
		}
	};

	private final OnImageAvailableListener imageListener = new OnImageAvailableListener()
	{
		@Override
		public void onImageAvailable(ImageReader reader_)
		{
			// get image
			Image image = reader_.acquireLatestImage();

			// if image is ready
			long now = SystemClock.elapsedRealtime();
			if ((now - mCaptureTime >= DURATION_CAPTURE) && (mCameraDevice != null))
			{
				// read image data
				ByteBuffer buffer = image.getPlanes()[0].getBuffer();
				byte[] data = new byte[buffer.remaining()];
				buffer.get(data);

				// close image
				image.close();

				// invoke observers
				invokeObservers(data);
			}
			else
			{
				image.close();
			}
		}
	};

	/**
	 * destroy instance and release it's resources.
	 *
	 * @param creator_ object that was getting the instance.
	 */
	public void release(Object creator_)
	{
		if (BuildConfig.DEBUG && sNInstance == 0)
		{
			throw new Error(Messages.exceptionAlreadyReleased());
		}
		else
		{
			// release resources for given creator
			super.release(creator_);

			pictureTaken.detach(creator_);

			--sNInstance;
			if (sNInstance == 0)
			{
				// release resources of the Camera2Tool
				closeSession();

				super.release(this);
				sInstance = null;
			}
		}
	}
}
