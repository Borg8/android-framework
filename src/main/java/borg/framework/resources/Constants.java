package borg.framework.resources;

import borg.framework.holders.GlobalsHolder;

public final class Constants
{
	/** framework version **/
	public static final int VERSION = 0;

	/** framework timestamp **/
	public static final long TIMESTAMP = 1579860051000L;

	/** application package name. **/
	public static final String NAME_PACKAGE = GlobalsHolder.getContext().getPackageName();

	/** length of array size value **/
	public static final int SIZE_ARRAY_SIZE_LENGTH = 2;

	/** size of array uint8 value **/
	public static final int SIZE_UINT8 = 1;

	/** size of array int8 value **/
	public static final int SIZE_INT8 = 1;

	/** size of array uint16 value **/
	public static final int SIZE_UINT16 = 2;

	/** size of array int16 value **/
	public static final int SIZE_INT16 = 2;

	/** size of array uint32 value **/
	public static final int SIZE_UINT32 = 4;

	/** size of array int32 value **/
	public static final int SIZE_INT32 = 4;

	/** size of array uint64 value **/
	public static final int SIZE_UINT64 = 8;

	/** size of array int64 value **/
	public static final int SIZE_INT64 = 8;

	private Constants()
	{
		// private constructor to prevent instantiation.
	}
}
