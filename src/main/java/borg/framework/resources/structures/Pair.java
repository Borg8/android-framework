package borg.framework.resources.structures;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.resources.Constants;

public final class Pair<K extends Serializable, V extends Serializable> implements Serializable
{
	private static final long serialVersionUID = Constants.VERSION;

	public final K key;

	public final V value;

	public Pair(K key_, V value_)
	{
		key = key_;
		value = value_;
	}

	@Override
	@Contract(pure = true)
	@NonNull
	public String toString()
	{
		return "{k: " + key + ", v: " + value + "}";
	}

	@Override
	@Contract(pure = true)
	public boolean equals(@Nullable Object object_)
	{
		if (object_ != this)
		{
			if (object_ instanceof Pair)
			{
				//noinspection unchecked
				Pair<K, V> pair = (Pair<K, V>)object_;
				if (key != pair.key)
				{
					if ((key == null) || (key.equals(pair.key) == false))
					{
						return false;
					}
				}

				if (value != pair.value)
				{
					return (value != null) && (value.equals(pair.value));
				}

				return true;
			}

			return false;
		}

		return true;
	}

	@Override
	@Contract(pure = true)
	public int hashCode()
	{
		int h1 = key == null? 0: key.hashCode();
		int h2 = value == null? 0: value.hashCode();

		return (h1 << 16) + h2;
	}
}
