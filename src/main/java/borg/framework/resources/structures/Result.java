package borg.framework.resources.structures;

import org.jetbrains.annotations.Contract;

import java.text.MessageFormat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Result<T extends Enum<T>>
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** result type **/
	@NonNull
	public final T type;

	/** object that create the result **/
	@NonNull
	public final Object actor;

	/** result free text **/
	@Nullable
	public final String message;

	/** result that causes the result **/
	@Nullable
	public final Result<? extends Enum<?>> cause;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public Result(@NonNull Object actor_, @NonNull T type_)
	{
		this(actor_, type_, null, null);
	}

	public Result(@NonNull Object actor_, @NonNull T type_, @Nullable String message_)
	{
		this(actor_, type_, message_, null);
	}

	public Result(@NonNull Object actor_,
		@NonNull T type_,
		@Nullable Result<? extends Enum<?>> cause_)
	{
		this(actor_, type_, null, cause_);
	}

	public Result(@NonNull Object actor_,
		@NonNull T type_,
		@Nullable String message_,
		@Nullable Result<? extends Enum<?>> cause_)
	{
		type = type_;
		actor = actor_;
		message = message_;
		cause = cause_;
	}

	@Override
	@Contract(pure = true)
	@NonNull
	public String toString()
	{
		StringBuilder builder = new StringBuilder();

		Result<? extends Enum<?>> result = this;
		for (; ; )
		{
			// append current result
			builder.append(MessageFormat.format("actor: {0}, type: {1} ({2}: {3})",
				result.actor.getClass().getSimpleName(),
				result.type.ordinal(),
				result.type.getDeclaringClass().getSimpleName(),
				result.type.name()));

			// if message is defined
			if (result.message != null)
			{
				builder.append(", message: ");
				builder.append(result.message);
			}

			// if cause exists
			if (result.cause != null)
			{
				// append result-cause connector
				builder.append(" <- ");
			}
			else
			{
				break;
			}

			// get cause
			result = result.cause;
		}

		return builder.toString();
	}
}
