package borg.framework.resources.structures;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

import androidx.annotation.NonNull;
import borg.framework.resources.Constants;

public final class Mac implements Serializable
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = Constants.VERSION;

	/** length of MAC address in bytes **/
	public static final int LENGTH_MAC_BYTES = 6;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** number of colons if valid MAC address **/
	private static final int NUM_COLONS = LENGTH_MAC_BYTES - 1;

	/** length of canonical MAC address string **/
	private static final int LENGTH_MAC_STRING = LENGTH_MAC_BYTES * 2 + NUM_COLONS;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** MAC value as long **/
	public final long numberValue;

	/** MAC address represented as canonical MAC **/
	public final String value;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public Mac(String mac_)
	{
		numberValue = getAsNumberValue(mac_);
		value = getAsCanonical(numberValue);
	}

	public Mac(long mac_)
	{
		numberValue = mac_;
		value = getAsCanonical(numberValue);
	}

	/**
	 * retrieve MAC as long value
	 *
	 * @param mac_ - MAC address
	 *
	 * @return MAC address as long value.
	 */
	@Contract(pure = true)
	public static long getAsNumberValue(@NonNull String mac_)
	{
		mac_ = mac_.trim();
		long mac = 0;

		// parse all bytes from MAC
		int n = mac_.length();
		int b = 0;
		int digNum = 0;
		for (int i = 0; i < n; ++i)
		{
			// get character from MAC
			char c = mac_.charAt(i);

			// compute value of character as hex digit
			int d;
			if ((c >= '0') && (c <= '9'))
			{
				d = c - '0';
			}
			else
			{
				if ((c >= 'a') && (c <= 'f'))
				{
					d = c - 'a' + 0xa;
				}
				else
				{
					if ((c >= 'A') && (c <= 'F'))
					{
						d = c - 'A' + 0xa;
					}
					else
					{
						d = -1;
					}
				}
			}

			// if character if valid hex digit
			if (d >= 0)
			{
				// update current byte
				b = (b << 4) + d;
				++digNum;
			}
			else
			{
				// if new byte was not started
				if (digNum > 0)
				{
					digNum = 2;
				}
			}

			// if current byte cannot contain more digits
			if (digNum == 2)
			{
				// update MAC
				mac = (mac << 8) + b;

				// reset current byte
				b = 0;
				digNum = 0;
			}
		}

		// if last digit was not read
		if (digNum > 0)
		{
			// update mac address with last byte
			mac = (mac << 8) + b;
		}

		return mac;
	}

	/**
	 * convert MAC number value to canonical MAC string.
	 *
	 * @param mac_ - given MAC number value.
	 *
	 * @return canonical MAC.
	 */
	@NonNull
	@Contract(pure = true)
	public static String getAsCanonical(long mac_)
	{
		char[] canonical = new char[LENGTH_MAC_STRING];

		for (int i = LENGTH_MAC_STRING - 1; i >= 0; )
		{
			// put first digit
			canonical[i] = createHexDigit((int)(mac_ & 0xf));
			--i;
			mac_ >>= 4;

			// put second digit
			canonical[i] = createHexDigit((int)(mac_ & 0xf));
			--i;
			mac_ >>= 4;

			// put colon
			if (i > 0)
			{
				canonical[i] = ':';
				--i;
			}
		}

		return new String(canonical);
	}

	/**
	 * convert MAC address represents as long to byte array.
	 *
	 * @param mac_ - MAC address.
	 *
	 * @return array represents the MAC.
	 */
	@Contract(pure = true)
	@NonNull
	public static byte[] getAsArray(long mac_)
	{
		byte[] mac = new byte[LENGTH_MAC_BYTES];
		for (int i = LENGTH_MAC_BYTES - 1; i >= 0; --i)
		{
			mac[i] = (byte)mac_;
			mac_ >>= 8;
		}

		return mac;
	}

	@Contract(pure = true)
	private static char createHexDigit(int digit_)
	{
		if (digit_ < 0xa)
		{
			return (char)(digit_ + '0');
		}

		return (char)(digit_ - 0xa + 'A');
	}

	@Contract(value = "null -> false", pure = true)
	@Override
	public boolean equals(Object o_)
	{
		if (o_ instanceof Mac)
		{
			return ((Mac)o_).numberValue == numberValue;
		}

		return false;
	}

	@Contract(pure = true)
	public int hashCode()
	{
		return (int)numberValue;
	}

	@Contract(pure = true)
	@Override
	@NonNull
	public String toString()
	{
		return value;
	}
}
