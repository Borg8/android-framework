package borg.framework.resources.structures;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.resources.Constants;

public final class PhoneNumber implements Serializable
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public static final PhoneNumber PRIVATE = new PhoneNumber("");

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = Constants.VERSION;

	private static final int LENGTH_GENERAL_NUMBER = 8;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** phone number **/
	@NonNull
	public final String number;

	/** normalized phone number **/
	@NonNull
	private final String mNormalizedNumber;

	/** phone number hash code **/
	private final int mHash;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public PhoneNumber(@Nullable String number_)
	{
		if (number_ == null)
		{
			number_ = PRIVATE.number;
		}

		number = number_;

		mNormalizedNumber = normalize(number_);
		mHash = mNormalizedNumber.hashCode();
	}

	@Override
	@Contract(value = "null -> false", pure = true)
	public boolean equals(Object object_)
	{
		if (object_ instanceof PhoneNumber)
		{
			return mNormalizedNumber.equals(((PhoneNumber)object_).mNormalizedNumber);
		}

		return false;
	}

	@Contract(pure = true)
	@Override
	public int hashCode()
	{
		return mHash;
	}

	@Override
	@Contract(pure = true)
	@NonNull
	public String toString()
	{
		return number;
	}

	@Contract(value = "null -> null", pure = true)
	private static String normalize(@Nullable String number_)
	{
		if (number_ != null)
		{
			// remove all non digits symbols
			//noinspection ResultOfMethodCallIgnored
			number_.replaceAll("[^0-9.]", "");

			// if number is general
			int len = number_.length();
			if (len > LENGTH_GENERAL_NUMBER)
			{
				number_ = number_.substring(len - LENGTH_GENERAL_NUMBER);
			}
		}

		return number_;
	}
}