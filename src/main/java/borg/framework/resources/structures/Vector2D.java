package borg.framework.resources.structures;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;
import java.text.MessageFormat;

import androidx.annotation.NonNull;
import borg.framework.resources.Constants;

public class Vector2D implements Serializable
{
	private static final long serialVersionUID = Constants.VERSION;

	private static final double HALF_PI = Math.PI / 2;

	/** x component **/
	public double x;

	/** y component **/
	public double y;

	/** x value for which size was computed **/
	private double mSizeX;

	/** y value for which size was computed **/
	private double mSizeY;

	/** last computed size **/
	private double mLastSize;

	/** x value for which direction was computed **/
	private double mDirX;

	/** y value for which direction was computed **/
	private double mDirY;

	/** last computed direction **/
	private double mLastDirection;

	public Vector2D()
	{
		x = 0;
		y = 0;
		mSizeX = 0;
		mSizeY = 0;
		mDirX = 0;
		mDirY = 0;
		mLastSize = 0;
		mLastDirection = HALF_PI;
	}

	public Vector2D(double x_, double y_)
	{
		x = x_;
		y = y_;
		mSizeX = x_ + 1;
		mDirX = mSizeX;
	}

	/**
	 * update vector coordinates from given vector.
	 *
	 * @param vector_ given vector.
	 */
	public final void update(@NonNull Vector2D vector_)
	{
		x = vector_.x;
		y = vector_.y;
	}

	/**
	 * @return vector direction in radians. 0 radians when (x == 0) && (y < 0).
	 */
	@Contract(pure = true)
	public final double getDirection()
	{
		// if vector coordinates was changed
		if ((x != mDirX) || (y != mDirY))
		{
			if (y == 0)
			{
				if (x < 0)
				{
					mLastDirection = -HALF_PI;
				}
				else
				{
					mLastDirection = HALF_PI;
				}
			}
			else
			{
				double angle = Math.atan(x / y);
				mLastDirection = y < 0? -angle: Math.PI - angle;
			}

			// store vector coordinates
			mDirX = x;
			mDirY = y;
		}

		return mLastDirection;
	}

	/**
	 * @return vector length.
	 */
	@Contract(pure = true)
	public final double getSize()
	{
		// if vector coordinates was changed
		if ((x != mSizeX) || (y != mSizeY))
		{
			// compute size
			mLastSize = Math.sqrt(x * x + y * y);

			// store vector coordinates
			mSizeX = x;
			mSizeY = y;
		}

		return mLastSize;
	}

	/**
	 * add vector to this one.
	 *
	 * @param vector_ vector two add.
	 *
	 * @return this vector after the addition.
	 */
	@Contract(pure = true)
	public final Vector2D add(@NonNull Vector2D vector_)
	{
		x += vector_.x;
		y += vector_.y;

		return this;
	}

	/**
	 * scalar product of vector.
	 *
	 * @param scalar_ scalar to product.
	 *
	 * @return this vector after production.
	 */
	@Contract(pure = true)
	public final Vector2D product(double scalar_)
	{
		x *= scalar_;
		y *= scalar_;

		return this;
	}

	/**
	 * dotting vector by given vector.
	 *
	 * @param vector_ given vector.
	 *
	 * @return dotting result.
	 */
	@Contract(pure = true)
	public final double product(@NonNull Vector2D vector_)
	{
		return (x * vector_.x) + (y * vector_.y);
	}

	/**
	 * resize the vector to given size.
	 *
	 * @param size_ size of vector after resizing.
	 *
	 * @return previous vector size.
	 */
	public final double resize(double size_)
	{
		double r = getSize();

		if (r > 0)
		{
			// get scale factor
			double factor = size_ / r;

			// change vector components
			x *= factor;
			y *= factor;
		}

		return r;
	}

	/**
	 * rotate vector by given angle.
	 *
	 * @param angle_ angle to rotate.
	 */
	public final void rotate(double angle_)
	{
		double temp = x;
		double cos = Math.cos(angle_);
		double sin = Math.sin(angle_);

		x = x * cos - y * sin;
		y = temp * sin + y * cos;
	}

	@Override
	@Contract(pure = true)
	@NonNull
	public String toString()
	{
		return MessageFormat.format("x = {0}, y = {1}\nsize = {2}, angle = {3}",
			Double.toString(x),
			Double.toString(y),
			getSize(),
			getDirection());
	}
}
