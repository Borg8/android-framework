package borg.framework.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

public abstract class SqlDatabase extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public enum Bool
	{
		/** false value **/
		FALSE(0, false),

		/** true value **/
		TRUE(1, true);

		/** integer represents the boolean **/
		public final int intValue;

		/** boolean value **/
		public final boolean boolValue;

		Bool(int int_, boolean boolean_)
		{
			intValue = int_;
			boolValue = boolean_;
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public interface Columns
	{
		/**
		 * @return column name.
		 */
		@NonNull
		@Contract(pure = true)
		String getName();
	}

	public static final class RowData
	{
		@NonNull
		public final String[] columns;

		@NonNull
		public final HashMap<Columns, Integer> indexes;

		public RowData(@NonNull Columns... columns_)
		{
			// create columns
			columns = new String[columns_.length];
			for (int i = 0; i < columns_.length; ++i)
			{
				columns[i] = columns_[i].getName();
			}

			// index columns
			indexes = new HashMap<>(columns_.length);
			for (int i = 0; i < columns_.length; ++i)
			{
				indexes.put(columns_[i], i);
			}
		}
	}

	private static final class DatabaseHelper extends SQLiteOpenHelper
	{
		/** command to create database **/
		private String[] mCreateCommands;

		public DatabaseHelper(@NonNull String database_, int version_)
		{
			super(GlobalsHolder.getContext(), database_, null, version_);
		}

		@Override
		public void onCreate(SQLiteDatabase db_)
		{
			for (String command : mCreateCommands)
			{
				db_.execSQL(command);
			}
		}

		@Override
		public void onConfigure(SQLiteDatabase db_)
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
			{
				db_.setForeignKeyConstraintsEnabled(true);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db_, int oldVersion_, int newVersion_)
		{
			Logger.log(MessageFormat.format("database: {0} upgraded: {1} -> {2}",
				db_.getPath(),
				oldVersion_,
				newVersion_));

			recreate(db_);
		}

		@Override
		public void onDowngrade(SQLiteDatabase db_, int oldVersion_, int newVersion_)
		{
			Logger.log(MessageFormat.format("database: {0} downgraded: {1} -> {2}",
				db_.getPath(),
				oldVersion_,
				newVersion_));

			recreate(db_);
		}

		@NonNull
		public SQLiteDatabase createDatabase(@NonNull String[] command_, boolean isWritable_)
		{
			mCreateCommands = command_;
			if (isWritable_ == true)
			{
				return getWritableDatabase();
			}
			return getReadableDatabase();
		}

		@SuppressWarnings("MethodCallInLoopCondition")
		private void recreate(@NonNull SQLiteDatabase db_)
		{
			// remove tables
			Cursor cursor = db_.query("sqlite_master",
				new String[] { "name" },
				"type='table'",
				null,
				null,
				null,
				null);
			while (cursor.moveToNext() == true)
			{
				String name = readValue(cursor, 0, (String)null);
				db_.execSQL("drop table " + name);
			}
			cursor.close();

			// create new database
			onCreate(db_);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** files database **/
	protected final SQLiteDatabase database;

	/** content values **/
	protected final ContentValues values;

	/** database helper instance **/
	private final DatabaseHelper mDatabaseHelper;

	/** fair lock **/
	private final ReentrantLock mLock;

	/** number of started transactions **/
	private int mTransactionsNum;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * constructor.
	 *
	 * @param name_       path to database file.
	 * @param version_    database version.
	 * @param command_    database creation command.
	 * @param isWritable_ if true then writable database will be created, read only otherwise.
	 */
	public SqlDatabase(@NonNull String name_,
		int version_,
		@NonNull String[] command_,
		boolean isWritable_)
	{
		mDatabaseHelper = new DatabaseHelper(name_, version_);

		values = new ContentValues();
		mTransactionsNum = 0;

		// create database
		database = mDatabaseHelper.createDatabase(command_, isWritable_);

		mLock = new ReentrantLock();
	}

	/**
	 * insert current values into database.
	 *
	 * @param table_ name of table to insert values to.
	 *
	 * @return the row ID of the newly inserted row, or -1 if an error occurred.
	 */
	public long insert(@NonNull String table_)
	{
		try
		{
			return database.insertOrThrow(table_, null, values);
		}
		catch (Exception e)
		{
			Logger.log(Log.ERROR, "table: " + table_ + ", values: " + values, e);
		}

		return -1;
	}

	/**
	 * query rows from table.
	 *
	 * @param table_     name of table to query from.
	 * @param columns_   columns to query.
	 * @param selection_ query selection.
	 *
	 * @return cursor to queried data. Shall be closed.
	 */
	@Contract(pure = true)
	@NonNull
	protected Cursor query(@NonNull String table_,
		@Nullable RowData columns_,
		@Nullable String selection_)
	{
		return query(table_, columns_, selection_, null, null);
	}

	/**
	 * query rows from table.
	 *
	 * @param table_     name of table to query from.
	 * @param columns_   columns to query.
	 * @param selection_ query selection.
	 * @param order_     query order.
	 *
	 * @return cursor to queried data. Shall be closed.
	 */
	@Contract(pure = true)
	@NonNull
	protected Cursor query(@NonNull String table_,
		@Nullable RowData columns_,
		@Nullable String selection_,
		@NonNull String order_)
	{
		return query(table_, columns_, selection_, order_, null);
	}

	/**
	 * query rows from table.
	 *
	 * @param table_     name of table to query from.
	 * @param columns_   columns to query.
	 * @param selection_ query selection.
	 * @param order_     query order.
	 * @param limit_     query limit.
	 *
	 * @return cursor to queried data. Shall be closed.
	 */
	@Contract(pure = true)
	@NonNull
	protected Cursor query(@NonNull String table_,
		@Nullable RowData columns_,
		@Nullable String selection_,
		@Nullable String order_,
		@Nullable String limit_)
	{
		String[] columns = columns_ == null? null: columns_.columns;
		return database.query(table_, columns, selection_, null, null, null, order_, limit_);
	}

	/**
	 * update rows with current values.
	 *
	 * @param table_ name of table where update.
	 * @param where_ rows condition.
	 *
	 * @return number of affected rows.
	 */
	protected int update(@NonNull String table_, @Nullable String where_)
	{
		int rows = database.update(table_, values, where_, null);
		if (rows == 0)
		{
			Logger.snapshot(Log.WARN,
				"nothing updated",
				"table",
				table_,
				"values",
				values,
				"where",
				where_);
		}

		return rows;
	}

	/**
	 * delete rows.
	 *
	 * @param table_ name of table where delete rows.
	 * @param where_ rows condition.
	 *
	 * @return number of deleted rows.
	 */
	protected int delete(@NonNull String table_, @Nullable String where_)
	{
		return database.delete(table_, where_, null);
	}

	/**
	 * read Integer value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	public static long readValue(@NonNull Cursor cursor_, int index_, long default_)
	{
		String temp = cursor_.getString(index_);
		if (temp != null)
		{
			try
			{
				return Long.parseLong(temp);
			}
			catch (Exception e_)
			{
				// nothing to do here
			}
		}

		return default_;
	}

	/**
	 * read Real value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	public static double readValue(@NonNull Cursor cursor_, int index_, double default_)
	{
		String temp = cursor_.getString(index_);
		if (temp != null)
		{
			try
			{
				return Double.parseDouble(temp);
			}
			catch (Exception e_)
			{
				// nothing to do here
			}
		}

		return default_;
	}

	/**
	 * read Boolean value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	public static boolean readValue(@NonNull Cursor cursor_, int index_, boolean default_)
	{
		long value = readValue(cursor_, index_, -1);
		if (value == Bool.FALSE.intValue)
		{
			return false;
		}

		if (value >= Bool.TRUE.intValue)
		{
			return true;
		}

		return default_;
	}

	/**
	 * read String value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	public static String readValue(@NonNull Cursor cursor_, int index_, @Nullable String default_)
	{
		String temp = cursor_.getString(index_);
		if (temp != null)
		{
			return temp;
		}

		return default_;
	}

	/**
	 * read byte array value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	public static byte[] readValue(@NonNull Cursor cursor_, int index_, @Nullable byte[] default_)
	{
		byte[] temp = cursor_.getBlob(index_);
		if (temp != null)
		{
			return temp;
		}

		return default_;
	}

	/**
	 * read Enum value from cursor.
	 *
	 * @param cursor_  cursor to read value from.
	 * @param index_   value index.
	 * @param default_ default if value can not be read.
	 *
	 * @return read value.
	 */
	@Contract(pure = true)
	@NonNull
	public static <T extends Enum<T>> T readValue(@NonNull Cursor cursor_,
		int index_,
		@NonNull T default_)
	{
		try
		{
			// get values
			Method method = default_.getClass().getMethod("values");
			//noinspection unchecked
			T[] values = (T[])method.invoke(null);

			// read value
			assert values != null;
			return values[(int)readValue(cursor_, index_, default_.ordinal())];
		}
		catch (Exception e)
		{
			Logger.log(e);
		}

		return default_;
	}

	/**
	 * begin transaction.
	 */
	public final void beginTransaction()
	{
		// clock lock for the thread
		mLock.lock();

		if (mTransactionsNum == 0)
		{
			// begin transaction
			database.beginTransaction();
		}

		++mTransactionsNum;

		Logger.snapshot(Log.VERBOSE, "begin transaction: " + mTransactionsNum);
	}

	/**
	 * commit transaction.
	 */
	public final void commitTransaction()
	{
		Logger.snapshot(Log.VERBOSE, "commit transaction: " + mTransactionsNum);

		--mTransactionsNum;

		if (mTransactionsNum == 0)
		{
			// end transaction
			database.setTransactionSuccessful();
			database.endTransaction();
		}

		// unlock
		mLock.unlock();
	}

	/**
	 * release database manager and all its resources.
	 */
	public void release()
	{
		mDatabaseHelper.close();
	}
}
