package borg.framework.auxiliaries;

import org.jetbrains.annotations.Contract;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.holders.GlobalsHolder;
import borg.framework.services.TimeManager;

public final class TextParser
{

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final String DAY = "d";

	private static final String HOUR = "h";

	private static final String MINUTE = "m";

	private static final String SECOND = "s";

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final DecimalFormat sDecimalFormat;

	private static Calendar sCalendar = null;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	static
	{
		sDecimalFormat = new DecimalFormat("#.##########", new DecimalFormatSymbols(Locale.US));
	}

	private TextParser()
	{
		// private constructor to prevent instantiation
	}

	/**
	 * get calendar initialized with given time.
	 *
	 * @param time_ time to initialize calendar for.
	 *
	 * @return calendar in local time zone initialize with given time.
	 */
	@NonNull
	@Contract(pure = true)
	public static Calendar getCalendar(long time_)
	{
		Calendar calendar = sCalendar;

		// if calendar not set
		if (sCalendar == null)
		{
			// if timezone set
			Integer offset = GlobalsHolder.getTimezone();
			if (offset != null)
			{
				// get timezone name
				String name;
				if (offset < 0)
				{
					name = "GMT-";
					offset = -offset;
				}
				else
				{
					name = "GMT+";
				}
				name += offset / TimeManager.HOUR;
				long minutes = offset % TimeManager.HOUR;
				if (minutes != 0)
				{
					name += ":" + (minutes / TimeManager.MINUTE);
				}

				// create calendar
				sCalendar = Calendar.getInstance();
				sCalendar.setTimeZone(TimeZone.getTimeZone(name));

				calendar = sCalendar;
			}
			else
			{
				// create temporary calendar
				calendar = Calendar.getInstance();
			}
		}

		// set time
		calendar.setTimeInMillis(time_);

		return calendar;
	}

	/**
	 * convert seconds to human time.
	 *
	 * @param timestamp_ given timestamp in milliseconds.
	 *
	 * @return string represents the seconds in human time.
	 */
	@NonNull
	@Contract(pure = true)
	public static String timestampToTime(long timestamp_)
	{
		if (timestamp_ < 0)
		{
			timestamp_ = -timestamp_;
		}

		long mill = timestamp_ % 1000;
		timestamp_ /= 1000;

		int days = (int)(timestamp_ / (24 * 60 * 60));
		timestamp_ %= 24 * 60 * 60;

		int hours = (int)timestamp_ / (60 * 60);
		timestamp_ %= 60 * 60;

		int minutes = (int)(timestamp_ / 60);
		timestamp_ %= 60;

		StringBuilder builder = new StringBuilder();
		if (days > 0)
		{
			builder.append(days);
			builder.append(DAY);
			builder.append(' ');
		}

		if (hours > 0)
		{
			builder.append(hours);
			builder.append(HOUR);
			builder.append(' ');
		}

		if (minutes > 0)
		{
			builder.append(minutes);
			builder.append(MINUTE);
			builder.append(' ');
		}

		builder.append(timestamp_);

		builder.append('.');
		if (mill < 100)
		{
			builder.append('0');
		}
		if (mill < 10)
		{
			builder.append('0');
		}

		builder.append(mill);
		builder.append(SECOND);

		return builder.toString();
	}

	private static final StringBuilder cBuilder = new StringBuilder();

	/**
	 * convert time stamp to date in human representation.
	 *
	 * @param timestamp_     time stamp to convert.
	 * @param timeSeparator_ separator between time parts. If {@code \0} provided, then time will not
	 *                       be included.
	 * @param dateSeparator_ separator between date parts. If {@code \0} provided, then date will not
	 *                       be included.
	 * @param precision_     required precision, from {@link Calendar#YEAR} to {@link Calendar#MILLISECOND}.
	 *
	 * @return full date in human representation.
	 */
	@NonNull
	@Contract(pure = true)
	public static String getHumanDate(long timestamp_,
		char timeSeparator_,
		char dateSeparator_,
		int precision_)
	{
		return getHumanDate(timestamp_, timeSeparator_, dateSeparator_, precision_, null);
	}

	/**
	 * convert time stamp to date in human representation.
	 *
	 * @param timestamp_     time stamp to convert.
	 * @param timeSeparator_ separator between time parts. If {@code \0} provided, then time will not
	 *                       be included.
	 * @param dateSeparator_ separator between date parts. If {@code \0} provided, then date will not
	 *                       be included.
	 * @param precision_     required precision, from {@link Calendar#YEAR} to {@link Calendar#MILLISECOND}.
	 * @param calendar_      calendar to use.
	 *
	 * @return full date in human representation.
	 */
	@NonNull
	@Contract(pure = true)
	public static String getHumanDate(long timestamp_,
		char timeSeparator_,
		char dateSeparator_,
		int precision_,
		@Nullable Calendar calendar_)
	{
		Calendar calendar = calendar_;
		if (calendar_ == null)
		{
			calendar = getCalendar(timestamp_);
		}
		else
		{
			calendar.setTimeInMillis(timestamp_);
		}

		cBuilder.setLength(0);

		// if date shall be included
		if (dateSeparator_ != '\0')
		{
			if (precision_ >= Calendar.DAY_OF_MONTH)
			{
				cBuilder.append(calendar.get(Calendar.DAY_OF_MONTH));
				cBuilder.append(dateSeparator_);
			}
			if (precision_ >= Calendar.MONTH)
			{
				cBuilder.append(calendar.get(Calendar.MONTH) + 1);
				cBuilder.append(dateSeparator_);
			}
			cBuilder.append(calendar.get(Calendar.YEAR));
			if (timeSeparator_ != '\0')
			{
				cBuilder.append(' ');
			}
		}

		// if time shall be included
		if (timeSeparator_ != '\0')
		{
			int value = calendar.get(Calendar.HOUR_OF_DAY);
			if (value < 10)
			{
				cBuilder.append('0');
			}
			cBuilder.append(value);
			if (precision_ >= Calendar.MINUTE)
			{
				cBuilder.append(timeSeparator_);
				value = calendar.get(Calendar.MINUTE);
				if (value < 10)
				{
					cBuilder.append('0');
				}
				cBuilder.append(value);
			}
			if (precision_ >= Calendar.SECOND)
			{
				cBuilder.append(timeSeparator_);
				value = calendar.get(Calendar.SECOND);
				if (value < 10)
				{
					cBuilder.append('0');
				}
				cBuilder.append(value);
			}
			if (precision_ >= Calendar.MILLISECOND)
			{
				cBuilder.append('.');
				cBuilder.append('0');
				if (value < 10)
				{
					cBuilder.append('0');
				}
				cBuilder.append(value);
			}
		}

		return cBuilder.toString();
	}

	/**
	 * convert double to string. Up to 10 digits after floating point.
	 *
	 * @param d_ double to convert.
	 *
	 * @return string represents the double.
	 */
	@Contract(pure = true)
	@NonNull
	public static String toString(double d_)
	{
		return sDecimalFormat.format(d_);
	}

	/**
	 * parse decimal integer from string.
	 *
	 * @param string_ string to parse.
	 *
	 * @return integer, if parsed.
	 */
	@Nullable
	@Contract(pure = true)
	public static Long parseInteger(@NonNull String string_)
	{
		// is string is empty
		int n = string_.length();
		if (n > 0)
		{
			// parse sign
			boolean minus = string_.charAt(0) == '-';

			// build integer
			long result = 0;
			for (int i = minus? 1: 0; i < n; ++i)
			{
				// if noo-digit character found
				char c = string_.charAt(i);
				if ((c < '0') || (c > '9'))
				{
					return null;
				}

				// assume that the number is not too big
				result = (result * 10) + c - '0';
			}

			// assume comparison is faster than multiplication
			return minus? -result: result;
		}

		return null;
	}

	/**
	 * get age.
	 *
	 * @param birthday_ birthday in milliseconds.
	 *
	 * @return age in years.
	 */
	@Contract(pure = true)
	public static int getAge(long birthday_)
	{
		// get birth date
		Calendar calendar = getCalendar(birthday_);
		int birthYear = calendar.get(Calendar.YEAR);
		int birthDays = calendar.get(Calendar.DAY_OF_YEAR);

		// get current date
		calendar = getCalendar(TimeManager.getRealTime());
		int year = calendar.get(Calendar.YEAR);
		int days = calendar.get(Calendar.DAY_OF_YEAR);

		// compute age
		int age = year - birthYear;
		if (birthDays > days)
		{
			--age;
		}

		return age;
	}

	/**
	 * capitalize sentence.
	 *
	 * @param sentence_ sentence to capitalize.
	 *
	 * @return sentence with first capital letter. Any other letters will not be changed.
	 */
	@NonNull
	@Contract(pure = true)
	public static String capitalize(@NonNull String sentence_)
	{
		// capitalize first letter
		return sentence_.substring(0, 1).toUpperCase() + sentence_.substring(1);
	}
}
