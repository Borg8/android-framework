package borg.framework.auxiliaries;

import android.os.SystemClock;

import java.util.HashMap;

import borg.framework.services.TimeManager;

public final class Looper
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long ACCURACY_INTERVAL = 1000L;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final class Occurrence
	{
		/** occurrence interval **/
		public final long interval;

		/** last time when runnable ran **/
		public long lastOccurrence;

		/** runnable that shall run **/
		private final Runnable runnable;

		public Occurrence(Runnable runnable_, long interval_)
		{
			runnable = runnable_;
			interval = interval_;
			lastOccurrence = 0;
		}

		public void run()
		{
			runnable.run();
			lastOccurrence = SystemClock.elapsedRealtime();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** next timer tick occurred event **/
	public final Event<Object> timerTick;

	/** map from callbacks to callback occurrence **/
	private final HashMap<Runnable, Occurrence> mOccurrences;

	/** single instance of Looper. **/
	private static Looper sInstance = null;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private Looper()
	{
		timerTick = new Event<>();

		mOccurrences = new HashMap<>();
	}

	/**
	 * @return single instance of Looper.
	 */
	public static Looper getInstance()
	{
		if (sInstance == null)
		{
			// create single instance of Looper
			sInstance = new Looper();
		}

		return sInstance;
	}

	/**
	 * reserve timer.
	 *
	 * @param runnable_ runnable that will be executed when timer event occurred.
	 * @param interval_ reserve looper timer cycle.
	 */
	public void reserveTimer(Runnable runnable_, long interval_)
	{
		// create new occurrence
		Occurrence occurrence = new Occurrence(runnable_, interval_);

		// search occurrence that can be synchronized
		for (Occurrence scheduled : mOccurrences.values())
		{
			// compute difference between two occurrences
			long remainder;
			if (scheduled.interval > occurrence.interval)
			{
				remainder = scheduled.interval % occurrence.interval;
			}
			else
			{
				remainder = occurrence.interval % scheduled.interval;
			}

			// if occurrences may be synchronized
			if (remainder <= ACCURACY_INTERVAL)
			{
				// schedule occurrence together
				occurrence.lastOccurrence = scheduled.lastOccurrence;

				break;
			}
		}

		// add occurrence
		mOccurrences.put(runnable_, occurrence);

		TimeManager.postponeAction(0, scheduler);
	}

	/**
	 * release timer.
	 *
	 * @param runnable_ runnable to release timer for it.
	 */
	public void releaseTimer(Runnable runnable_)
	{
		// remove occurrence
		mOccurrences.remove(runnable_);

		// reschedule timer
		TimeManager.postponeAction(0, scheduler);
	}

	private final TimeManager.TimeHandler scheduler = new TimeManager.TimeHandler()
	{
		@Override
		public void onCallback()
		{
			// if timer event should occur
			if (mOccurrences.isEmpty() == false)
			{
				// get next occurrence
				long now = SystemClock.elapsedRealtime();
				long minLag = Long.MAX_VALUE;
				boolean isOccurred = false;
				for (Occurrence occurrence : mOccurrences.values())
				{
					// if runnable shall be executed
					long lag = occurrence.interval - (now - occurrence.lastOccurrence);
					if (lag - ACCURACY_INTERVAL <= 0)
					{
						occurrence.run();
						lag = occurrence.interval;
						isOccurred = true;
					}

					// if min lag found
					if (minLag > lag)
					{
						minLag = lag;
					}
				}

				// if timer event occurred
				if (isOccurred == true)
				{
					timerTick.invoke(null);
				}

				// schedule next cycle
				TimeManager.postponeAction(minLag, scheduler);
			}
		}
	};
}
