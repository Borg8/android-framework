package borg.framework.auxiliaries;

import android.util.Log;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.MessageFormat;
import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.BuildConfig;
import borg.framework.services.StorageManager;
import borg.framework.services.TimeManager;

public final class Logger
{
	@FunctionalInterface
	public interface Listener
	{
		/**
		 * log received.
		 *
		 * @param level_   log level.
		 * @param message_ log message.
		 */
		void log(int level_, @NonNull String message_);
	}

	private Logger()
	{
		// private constructor to avoid instantiation
	}

	/** application log prefix **/
	private static final String TAG_LOG = "~!";

	/** maximum exception causes depth **/
	private static final int MAX_EXCEPTION_CAUSE_DEPTH = 10;

	/** logs listener **/
	@Nullable
	private static Listener sListener;

	/** logs level to store **/
	private static int sLevel;

	/** file to store reports there **/
	@Nullable
	private static File sFile;

	/** package to start trace from **/
	@Nullable
	private static String sRootPackage;

	/** sign whether log already stored **/
	private static boolean sIsLogging;

	static
	{
		sLevel = 0;
		sFile = null;
		sIsLogging = false;
		sRootPackage = null;
		sListener = null;
	}

	/**
	 * set level of logging that will be stored to file.
	 *
	 * @param l_    log level according to {@link Log} constants. 0 to stop store reports.
	 * @param file_ file to store reports there.
	 */
	public static void setStoreLevel(int l_, @Nullable String file_)
	{
		if ((BuildConfig.DEBUG) && (file_ != null))
		{
			sLevel = l_;
			try
			{
				sFile = StorageManager.getFile(file_);
			}
			catch (Exception e)
			{
				throw new Error(e);
			}
		}
		else
		{
			sLevel = 0;
		}
	}

	/**
	 * set package from which stack trace will be built.
	 *
	 * @param package_ package to set.
	 */
	public static void setRootPackage(@Nullable String package_)
	{
		sRootPackage = package_;
	}

	/**
	 * set log listener.
	 *
	 * @param listener_ log listener to set.
	 */
	public static void setListener(@Nullable Listener listener_)
	{
		sListener = listener_;
	}

	/**
	 * log snapshot.
	 *
	 * @param level_   snapshot log level according to {@link Log} constants.
	 * @param message_ message to log.
	 * @param state_   snapshot variables state. Every odd object is an variable name, following even
	 *                 object is the variable value.
	 */
	public static void snapshot(int level_, @Nullable String message_, Object... state_)
	{
		// build stack trace
		Thread thread = Thread.currentThread();
		StackTraceElement[] stackTrace = thread.getStackTrace();
		String stack = buildStackTrace(stackTrace, 2);

		// build state
		String state = null;
		if (state_ != null)
		{
			int n = state_.length;
			if (n > 0)
			{
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < n; i += 2)
				{
					builder.append(state_[i]);
					builder.append(": ");
					builder.append(state_[i + 1]);
					builder.append("\n");
				}
				state = new String(builder);
			}
		}

		// build function
		String className = stackTrace[3].getClassName();
		String method = stackTrace[3].getMethodName();
		int line = stackTrace[3].getLineNumber();

		// build message
		String message = MessageFormat.format("thread: {0}({1}) - {2}#{3}:{4}{5}\n\n{6}\n{7}",
			thread.getName(),
			thread.getId(),
			className,
			method,
			Integer.toString(line),
			message_ == null? "": " - " + message_,
			state == null? "": state,
			stack);

		// log
		log(level_, message);
	}

	/**
	 * logging exception.
	 *
	 * @param e_ exception to logging
	 */
	public static void log(@NonNull Throwable e_)
	{
		log(Log.WARN, null, e_);
	}

	/**
	 * logging exception.
	 *
	 * @param level_ log level according to {@link Log} constants.
	 * @param e_     exception to logging
	 */
	public static void log(int level_, @NonNull Throwable e_)
	{
		log(level_, null, e_);
	}

	/**
	 * logging exception.
	 *
	 * @param level_   log level according to {@link Log} constants.
	 * @param message_ additional message.
	 * @param e_       exception to logging
	 */
	public static void log(int level_, @Nullable Object message_, @NonNull Throwable e_)
	{
		String message = MessageFormat.format("{0}{1}",
			message_ == null? "": "message: " + message_ + ": ",
			buildExceptionReport(e_));
		log(level_, message);
	}

	/**
	 * logging message
	 *
	 * @param message_ message to logging
	 */
	public static void log(@NonNull Object message_)
	{
		log(Log.VERBOSE, message_);
	}

	/**
	 * logging message
	 *
	 * @param level_   log level accordingly to {@link Log} constants
	 * @param message_ message to logging
	 */
	public static void log(int level_, @NonNull Object message_)
	{
		String message = message_.toString();

		switch (level_)
		{
			case Log.DEBUG:
				Log.d(TAG_LOG, message);
				break;

			case Log.INFO:
				Log.i(TAG_LOG, message);
				break;

			case Log.WARN:
				Log.w(TAG_LOG, message);
				break;

			case Log.ERROR:
				Log.e(TAG_LOG, message);
				break;

			default:
				Log.v(TAG_LOG, message);
				break;
		}

		// if log should be stored
		if ((sLevel > 0) && (level_ >= sLevel))
		{
			try
			{
				// if log shall be stored
				if (sIsLogging == false)
				{
					// sign that log is being stored
					sIsLogging = true;

					// create log
					message = TextParser.getHumanDate(TimeManager.getRealTime(),
						':',
						'.',
						Calendar.MILLISECOND) +
						" - " +
						message +
						"\n\n";

					// store log
					try
					{
						assert sFile != null;
						StorageManager.appendFile(sFile, message.getBytes());
					}
					catch (Exception e)
					{
						log(e);
					}
				}
			}
			catch (Exception e)
			{
				// give up
			}

			// sign that log was stored
			sIsLogging = false;
		}

		// invoke listener
		if (sListener != null)
		{
			sListener.log(level_, message);
		}
	}

	/**
	 * get stack trace as string.
	 *
	 * @param traceElements_ stack trace elements.
	 * @param start_         number of elements to skip.
	 *
	 * @return built string.
	 */
	@Contract(pure = true)
	@NonNull
	public static String buildStackTrace(@NonNull StackTraceElement[] traceElements_, int start_)
	{
		StringBuilder builder = new StringBuilder();

		// get build stack trace
		int n = traceElements_.length - 1;
		boolean isPackage = false;
		for (int i = start_ + 1; i < n; ++i)
		{
			StackTraceElement element = traceElements_[i];

			// if root package set
			if (sRootPackage != null)
			{
				// if not inside the package
				String className = element.getClassName();
				if ((className == null) || (className.startsWith(sRootPackage) == false))
				{
					// if quit from the package
					if (isPackage == true)
					{
						break;
					}
				}
				else
				{
					isPackage = true;
				}
			}

			// build stack element
			builder.append(element.getMethodName());
			builder.append('(');
			builder.append(element.getFileName());
			builder.append(':');
			builder.append(element.getLineNumber());
			builder.append(")\n");
		}

		return new String(builder);
	}

	/**
	 * build exception report.
	 *
	 * @param e_ exception.
	 *
	 * @return built string.
	 */
	@NonNull
	@Contract(pure = true)
	public static String buildExceptionReport(Throwable e_)
	{
		return buildExceptionReport(Thread.currentThread(), e_);
	}

	/**
	 * build exception report.
	 *
	 * @param thread_ thread where exception was occurred.
	 * @param e_      exception.
	 *
	 * @return built string.
	 */
	@NonNull
	@Contract(pure = true)
	public static String buildExceptionReport(@NotNull Thread thread_, @NonNull Throwable e_)
	{
		StringBuilder builder = new StringBuilder();

		// append thread
		builder.append("thread: \"");
		builder.append(thread_.getName());
		builder.append('"');

		// append exception
		builder.append(", exception: ");
		builder.append(e_);

		// add causes
		int i = 0;
		Throwable cause = e_.getCause();
		while ((cause != null) && (i < MAX_EXCEPTION_CAUSE_DEPTH))
		{
			// append cause
			builder.append(", cause: ");
			builder.append(cause);

			// get next cause
			e_ = cause;
			cause = cause.getCause();
			++i;
		}

		// append stack trace
		builder.append('\n');
		builder.append(buildStackTrace(e_.getStackTrace(), 0));

		return builder.toString();
	}
}
