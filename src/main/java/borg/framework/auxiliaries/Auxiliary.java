package borg.framework.auxiliaries;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import org.jetbrains.annotations.Contract;

import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.pm.PackageInfoCompat;
import borg.framework.holders.GlobalsHolder;
import borg.framework.resources.Constants;
import borg.framework.services.ApplicationsManager;
import borg.framework.services.TimeManager;

public final class Auxiliary
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final class ToastHandler extends Handler
	{
		private static final int HANDLER_WAIT = 0;
		private static final int HANDLER_QUIT = 1;

		private final long mDuration;

		public ToastHandler(long duration_)
		{
			mDuration = duration_;
		}

		@Override
		public void handleMessage(@NonNull Message msg_)
		{
			switch (msg_.what)
			{
				case HANDLER_WAIT:
					SystemClock.sleep(mDuration - 1000);

					Message msg = new Message();
					msg.what = HANDLER_QUIT;
					sendMessageDelayed(msg, 5000);

					break;
				case HANDLER_QUIT:
					//noinspection ConstantConditions // cannot happened
					Looper.myLooper().quit();

					break;
				default:
					break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final TelephonyManager sTelephonyManager;

	private static final ToneGenerator sToneGenerator;

	private static final ClipboardManager sClipboardManager;

	private static final Random sRandom = new Random();

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	static
	{
		Context context = GlobalsHolder.getContext();
		sTelephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

		sToneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);

		sClipboardManager = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);

		resetRandom(TimeManager.getRealTime());
	}

	private Auxiliary()
	{
		// private constructor to prevent instantiation
	}

	/**
	 * initialize auxiliary. This method must be called from main thread.
	 */
	public static void init()
	{
		// if not running from main thread
		if (Looper.getMainLooper() != Looper.myLooper())
		{
			throw new Error(Messages.exceptionOnlyMainThread());
		}
	}

	/**
	 * get the application version.
	 */
	@Contract(pure = true)
	public static long getVersionCode()
	{
		PackageInfo info = ApplicationsManager.getPackageInfo(Constants.NAME_PACKAGE,
			PackageManager.GET_META_DATA);
		if (info != null)
		{
			return PackageInfoCompat.getLongVersionCode(info);
		}

		return -1;
	}

	/**
	 * reset random pseudo algorithm.
	 *
	 * @param seed_ initialization value.
	 */
	public static void resetRandom(long seed_)
	{
		sRandom.setSeed(seed_);
	}

	/**
	 * @return pseudo random unsigned numbers in 31 bit
	 */
	@Contract(pure = true)
	public static int random()
	{
		int value = sRandom.nextInt();
		return value < 0? -value: value;
	}

	/**
	 * show toast message
	 *
	 * @param message_  message text
	 * @param duration_ duration of toast showing. Value have to be larger than {@link Toast#LENGTH_SHORT} duration.
	 */
	public static void showToast(final String message_, final long duration_)
	{
		new Thread()
		{
			@Override
			public void run()
			{
				Looper.prepare();
				ToastHandler handler = new ToastHandler(duration_);

				Toast.makeText(GlobalsHolder.getContext(), message_, Toast.LENGTH_SHORT).show();

				Message msg = new Message();
				msg.what = ToastHandler.HANDLER_WAIT;
				handler.sendMessageDelayed(msg, 500);
				Looper.loop();
			}
		}.start();
	}

	/**
	 * does beep.
	 *
	 * @param toneType_ beep type according to {@link ToneGenerator} class.
	 */
	public static void beep(final int toneType_)
	{
		sToneGenerator.stopTone();
		sToneGenerator.startTone(toneType_);
	}

	/**
	 * @return data from clipboard.
	 */
	@Nullable
	@Contract(pure = true)
	public static CharSequence getFromClipboard()
	{
		// if primary clip available
		if (sClipboardManager.hasPrimaryClip() == true)
		{
			// if plain text data available
			ClipDescription description = sClipboardManager.getPrimaryClipDescription();
			if ((description != null) && (description.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)))
			{
				// return text
				ClipData clip = sClipboardManager.getPrimaryClip();
				if (clip != null)
				{
					return clip.getItemAt(0).getText();
				}
			}
		}

		return null;
	}

	/**
	 * store text to clipboard.
	 *
	 * @param label_ clipboard description.
	 * @param text_  text to store.
	 */
	public static void storeToClipboard(@Nullable CharSequence label_, @NonNull CharSequence text_)
	{
		sClipboardManager.setPrimaryClip(ClipData.newPlainText(label_, text_));
	}

	/**
	 * get URI to resource.
	 *
	 * @param id_ resource ID.
	 *
	 * @return URI to the resource.
	 */
	@NonNull
	@Contract(pure = true)
	public static Uri getUriToResource(int id_)
	{
		Resources resources = GlobalsHolder.getResources();

		return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
			"://" + resources.getResourcePackageName(id_) +
			'/' + resources.getResourceTypeName(id_) +
			'/' + resources.getResourceEntryName(id_));
	}

	/**
	 * @return phone unique identifier if available.
	 */
	@Contract(pure = true)
	@Nullable
	public static String getDeviceId()
	{
		String id = sTelephonyManager.getImei();
		if (id == null)
		{
			id = sTelephonyManager.getMeid();
		}

		return id;
	}

	/**
	 * try to get super user permission.
	 *
	 * @return {@code true} was able to ask for permission, {@code false} otherwise.
	 */
	@Contract(pure = true)
	public static boolean askSuPermission()
	{
		Runtime runtime = Runtime.getRuntime();
		try
		{
			runtime.exec("su");

			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	/**
	 * terminate application immediately.
	 */
	public static void terminate()
	{
		System.exit(0);
	}

	/**
	 * release resources.
	 */
	public static void release()
	{
		sToneGenerator.release();
	}

	/**
	 * get given part from BLE advertisement.
	 *
	 * @param advert_ advertisement to get from.
	 * @param type_   part ID.
	 *
	 * @return array with the part or {@code null} if the part was not found.
	 */
	@Nullable
	@Contract(pure = true)
	public static byte[] getAdvertPart(@NonNull byte[] advert_, byte type_)
	{
		byte[] part = null;

		int len = 0;
		int i = 0;
		while (i < advert_.length)
		{
			// if length is unknown
			if (len == 0)
			{
				// get part length
				len = advert_[i];

				// if length is valid
				if (len != 0)
				{
					++i;
				}
				else
				{
					break;
				}
			}
			else
			{
				// if part with given ID was found
				if (type_ == advert_[i])
				{
					// pass ID
					++i;
					--len;

					// create part
					part = new byte[len];
					System.arraycopy(advert_, i, part, 0, len);

					break;
				}

				// skip the part
				i += len;
				len = 0;
			}
		}

		return part;
	}
}