package borg.framework.auxiliaries;

import org.jetbrains.annotations.Contract;

import java.util.HashSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Borg
 */
public final class Event<T>
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public abstract static class Observer<T>
	{
		@NonNull
		public final Object owner;

		boolean isOnetime;

		/**
		 * @param owner_ instance where Observer was instantiated (usual it will be 'this').
		 */
		public Observer(@NonNull Object owner_)
		{
			owner = owner_;
		}

		/**
		 * observer action for the event.
		 *
		 * @param param_ event parameter.
		 */
		public abstract void action(T param_);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** list of observers attached to the event **/
	private final HashSet<Observer<T>> mObservers;

	/** clone list of observers to maintain the main list **/
	private final HashSet<Observer<T>> mObserversClone;

	/** sign whether observers list is dirty **/
	private boolean mIsObserversDirty;

	/** sign whether observers invocation is executed **/
	private boolean mIsDuringInvocation;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public Event()
	{
		mObservers = new HashSet<>();
		mObserversClone = new HashSet<>();
		mIsObserversDirty = false;
		mIsDuringInvocation = false;
	}

	/**
	 * @return number of observers that observe that event.
	 */
	@Contract(pure = true)
	public int getSize()
	{
		return mObservers.size();
	}

	/**
	 * attaches a new observer to the event.
	 *
	 * @param observer_ the attached observer.
	 */
	public void attach(@NonNull Observer<T> observer_)
	{
		attach(observer_, false);
	}

	/**
	 * attaches a new observer to the event. The observer will be detached after invocation.
	 *
	 * @param observer_ the attached observer.
	 */
	public void attachOnce(@NonNull Observer<T> observer_)
	{
		// if observer was attached
		attach(observer_, true);
	}

	private void attach(@NonNull Observer<T> observer_, boolean isOnetime_)
	{
		observer_.isOnetime = isOnetime_;

		// if observer was attached
		if (mObservers.add(observer_) == true)
		{
			// if invocation is not executed
			if (mIsDuringInvocation == false)
			{
				// add observer to observers clone list
				mObserversClone.add(observer_);
			}
			else
			{
				// sign observers list as dirty
				mIsObserversDirty = true;
			}
		}
	}

	/**
	 * detach attached method of specified observer.
	 *
	 * @param observer_ detached observer.
	 */
	public void detach(@NonNull Observer<T> observer_)
	{
		// if observer was removed
		if (mObservers.remove(observer_) == true)
		{
			// if invocation is not executed
			if (mIsDuringInvocation == false)
			{
				// remove observer from observers clone list
				mObserversClone.remove(observer_);
			}
			else
			{
				// sign observers list as dirty
				mIsObserversDirty = true;
			}
		}
	}

	/**
	 * removes all observer of given owner.
	 *
	 * @param owner_ instance of object that owned the removed observers.
	 */
	public void detach(@NonNull Object owner_)
	{
		// detach all observers of the owner from observers list
		for (Observer<T> observer : new HashSet<>(mObservers))
		{
			// if observer belongs to the owner
			if (observer.owner == owner_)
			{
				detach(observer);
			}
		}
	}

	/**
	 * detaches all observers from the event.
	 */
	public void detachAll()
	{
		mObservers.clear();

		// if invocation is not executed
		if (mIsDuringInvocation == false)
		{
			// remove all observers
			mObserversClone.clear();
		}
		else
		{
			// sign observers list as dirty
			mIsObserversDirty = true;
		}
	}

	/**
	 * invokes all attached observers.
	 *
	 * @param param_ parameters to pass to observers.
	 *
	 * @return first thrown exception or {@code null} if no exception thrown.
	 */
	@Nullable
	public Throwable invoke(@Nullable T param_)
	{
		boolean prevDuringInvocation = mIsDuringInvocation;

		// set that invocation is executed
		mIsDuringInvocation = true;

		// invokes attached observers
		Throwable exception = null;
		for (Observer<T> observer : mObserversClone)
		{
			// invoke method
			try
			{
				// if observer is one time
				if (observer.isOnetime == true)
				{
					detach(observer);
				}

				observer.action(param_);
			}
			catch (Throwable e)
			{
				// if first exception thrown
				if (exception == null)
				{
					exception = e;
				}

				// log exception
				Logger.log(e);
			}
		}

		// if observers list changed
		if (mIsObserversDirty == true)
		{
			// build new observer clone list
			mObserversClone.clear();
			mObserversClone.addAll(mObservers);
		}

		// rollback during invocation flag
		mIsDuringInvocation = prevDuringInvocation;

		return exception;
	}
}
