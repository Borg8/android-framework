package borg.framework.serializers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.auxiliaries.Logger;
import borg.framework.resources.Constants;

public class BinarySerialized extends Serialized
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = Constants.VERSION;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	protected BinarySerialized(@NonNull String tag_,
		@Nullable Encryptor encryptor_,
		@Nullable Serializer serializer_)
	{
		super(tag_, encryptor_, serializer_);
	}

	protected BinarySerialized(@NonNull String tag_, @Nullable Encryptor encryptor_)
	{
		this(tag_, encryptor_, null);
	}

	protected BinarySerialized(@NonNull String tag_, @Nullable Serializer serializer_)
	{
		this(tag_, null, serializer_);
	}

	protected BinarySerialized(@NonNull String tag_)
	{
		this(tag_, null, null);
	}

	@Override
	protected byte[] serialize()
	{
		try
		{
			// try to serialize object
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			ObjectOutputStream objectStream = new ObjectOutputStream(byteStream);

			// write the object
			objectStream.writeObject(this);
			objectStream.flush();
			objectStream.close();
			byteStream.close();

			// return serialized object
			return byteStream.toByteArray();
		}
		catch (Exception e)
		{
			Logger.log(e);
		}

		return null;
	}

	@Override
	protected Serialized deserialize(@NonNull byte[] data_)
	{
		try
		{
			// deserialize object
			ByteArrayInputStream stream = new ByteArrayInputStream(data_);
			Object object = new ObjectInputStream(stream).readObject();
			stream.close();

			return (BinarySerialized)object;
		}
		catch (Exception e)
		{
			Logger.log(e);
		}

		return null;
	}
}
