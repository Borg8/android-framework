package borg.framework.services;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.resources.Constants;

public final class BinaryWriter
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** stream to write to **/
	@NonNull
	private final List<Byte> mStream;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public BinaryWriter()
	{
		mStream = new ArrayList<>();
	}

	@NonNull
	@Contract(pure = true)
	public byte[] buildStream()
	{
		byte[] stream = new byte[mStream.size()];
		for (int i = 0; i < stream.length; ++i)
		{
			stream[i] = mStream.get(i);
		}

		return stream;
	}

	public int writeInt8(byte value_)
	{
		return writeInteger(value_, Constants.SIZE_INT8);
	}

	public int writeInt16(short value_)
	{
		return writeInteger(value_, Constants.SIZE_INT16);
	}

	public int writeInt32(int value_)
	{
		return writeInteger(value_, Constants.SIZE_INT32);
	}

	public int writeInt64(long value_)
	{
		return writeInteger(value_, Constants.SIZE_INT64);
	}

	public <T extends Enum<T>> int writeEnum(@NonNull T value_)
	{
		return writeInt8((byte)value_.ordinal());
	}

	public int writeInt8Array(@Nullable byte[] array_)
	{
		if (array_ == null)
		{
			array_ = new byte[0];
		}

		// write size
		int size = writeInt16((short)array_.length);

		// write elements
		for (byte b : array_)
		{
			size += writeInt8(b);
		}

		return size;
	}

	private int writeInteger(long integer_, int size_)
	{
		for (int i = 0; i < size_; ++i)
		{
			byte b = (byte)(integer_ & 0xff);
			integer_ >>= 8;
			mStream.add(b);
		}

		return size_;
	}
}
