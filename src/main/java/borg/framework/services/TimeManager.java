package borg.framework.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import androidx.annotation.NonNull;
import borg.framework.auxiliaries.Logger;
import borg.framework.auxiliaries.Looper;
import borg.framework.auxiliaries.Messages;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;
import borg.framework.resources.Constants;
import borg.framework.serializers.BinarySerialized;

public final class TimeManager extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** second duration **/
	public static final long SECOND = 1000;

	/** minute duration **/
	public static final long MINUTE = SECOND * 60;

	/** hour duration **/
	public static final long HOUR = MINUTE * 60;

	/** day duration **/
	public static final long DAY = HOUR * 24;

	/** week duration **/
	public static final long WEEK = DAY * 7;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final long MIN_ALARM_ACCURACY = MINUTE;

	private static final long INTERVAL_TIME_KEEPING = 15 * SECOND;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private static final class State extends BinarySerialized
	{
		private static final long serialVersionUID = Constants.VERSION;

		/** last received real time **/
		public long realTime;

		/** time elapsed from phone reboot when real time was computed **/
		public long elapsedTime;

		/** phone local time when real time was computed **/
		public long localTime;

		/** whether the clock is reliable **/
		public boolean isReliable;

		public State(String tag_)
		{
			super(tag_);
		}
	}

	public abstract static class TimeHandler
	{
		static int nextInstance = 0;

		final int instance;

		@Nullable
		Cancellable task = null;

		long time;

		public TimeHandler()
		{
			instance = nextInstance;
			++nextInstance;
		}

		/**
		 * handler execution callback.
		 */
		public abstract void onCallback();

		/**
		 * handler cancel callback.
		 */
		public void onCancel()
		{
			// nothing to do here
		}

		/**
		 * @return if the handler was scheduled, then how much time left until the handler will be
		 * executed, -1 otherwise.
		 */
		@Contract(pure = true)
		public final long getTimeout()
		{
			if (task != null)
			{
				long timeout = time - SystemClock.elapsedRealtime();
				return (timeout < 0)? 0: timeout;
			}

			return -1;
		}

		/**
		 * cancel execution.
		 */
		public final void cancel()
		{
			// if handler is not cancelled
			if (task != null)
			{
				// cancel task
				task.cancel();
				task = null;

				// call on cancel callback
				onCancel();
			}
		}

		private final void schedule(@NonNull Cancellable task_, long time_)
		{
			task = task_;
			time = time_;
		}

		private final void callback()
		{
			// sign handler as cancelled
			task = null;

			// call to call back
			onCallback();
		}
	}

	private interface Cancellable
	{
		void cancel();
	}

	private interface CancellableRunnable extends Runnable, Cancellable
	{
	}

	private abstract static class CancellableReceiver extends BroadcastReceiver implements Cancellable
	{
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** how much time the clock was not kept **/
	public static final long idle;

	/** handler for timers **/
	private static final Handler sHandler = new Handler();

	/** instance state **/
	private static final State sState;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	static
	{
		long local = System.currentTimeMillis();
		long elapsed = SystemClock.elapsedRealtime();

		// read state
		sState = new State(TimeManager.class.getName());
		if (sState.readState() == false)
		{
			sState.realTime = local;
			sState.elapsedTime = elapsed;
			sState.localTime = local;
			sState.isReliable = false;
		}

		// compute how much time clock was idle
		idle = local - sState.localTime;

		// if system elapsed time is not reliable
		if (elapsed <= sState.elapsedTime + idle)
		{
			recalculate();
		}

		// register receiver on time change
		Context context = GlobalsHolder.getContext();
		IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_CHANGED);
		context.registerReceiver(new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context_, Intent intent)
			{
				keepTime();
			}
		}, filter);

		// start looper
		Looper.getInstance().reserveTimer(TimeManager::keepTime, INTERVAL_TIME_KEEPING);
	}

	private TimeManager()
	{
		// private constructor to prevent instantiation
	}

	/**
	 * initialize reliable clock. This method must be called from main thread.
	 */
	public static void init()
	{
		// if not running from main thread
		if (android.os.Looper.getMainLooper() != android.os.Looper.myLooper())
		{
			throw new Error(Messages.exceptionOnlyMainThread());
		}
	}

	/**
	 * @return real reliable time in milliseconds.
	 */
	@Contract(pure = true)
	public static long getRealTime()
	{
		long time = (SystemClock.elapsedRealtime() - sState.elapsedTime);
		return sState.realTime + time;
	}

	/**
	 * convert elapsed time to real time.
	 *
	 * @param elapsedTime_ time to convert.
	 *
	 * @return real time for given moment that elapsed from phone reboot.
	 */
	@Contract(pure = true)
	public static long getRealTime(long elapsedTime_)
	{
		return sState.realTime + (elapsedTime_ - sState.elapsedTime);
	}

	/**
	 * @return whether the clock is reliable.
	 */
	@Contract(pure = true)
	public static boolean isReliable()
	{
		return sState.isReliable;
	}

	/**
	 * set real reliable time.
	 *
	 * @param time_ time to set.
	 */
	public static void setRealTime(long time_)
	{
		sState.realTime = time_;
		sState.elapsedTime = SystemClock.elapsedRealtime();
		sState.localTime = System.currentTimeMillis();
		sState.isReliable = true;
		sState.saveState();
	}

	/**
	 * recalculate real time based on system local time. Should be called after system reboot.
	 */
	public static void recalculate()
	{
		long local = System.currentTimeMillis();
		sState.realTime += local - sState.localTime;
		sState.localTime = local;
		sState.elapsedTime = SystemClock.elapsedRealtime();
		sState.isReliable = false;

		sState.saveState();
	}

	/**
	 * perform action with delay. If given action was already scheduled, then previous handler will be
	 * cancelled. Action will be executed on main thread.
	 *
	 * @param interval_ interval to invoke the alarm according to {@link AlarmManager} constants.
	 * @param handler_  handler that will handle the action.
	 */
	public static void scheduleAction(long interval_, @NonNull final TimeHandler handler_)
	{
		// create alarm manager
		final Context context = GlobalsHolder.getContext();
		final AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		assert manager != null;

		// create intent
		String action = TimeManager.class.getName() + ".alarm_" + handler_.instance;
		final PendingIntent intent = PendingIntent.getBroadcast(context, 0, new Intent(action), 0);

		// if handler already scheduled
		if (handler_.getTimeout() >= 0)
		{
			// cancel handler
			handler_.cancel();
		}

		// schedule handler
		final CancellableReceiver receiver = new CancellableReceiver()
		{
			@Override
			public void onReceive(Context context_, Intent intent_)
			{
				// if alarm should not be fired
				long interval = handler_.getTimeout();
				if (interval > MIN_ALARM_ACCURACY)
				{
					// reschedule the alarm
					interval += SystemClock.elapsedRealtime();
					manager.set(AlarmManager.ELAPSED_REALTIME, interval, intent);
				}
				else
				{
					try
					{
						// if handler was not cancelled or restarted
						if (handler_.task == this)
						{
							context_.unregisterReceiver(this);
							handler_.callback();
						}
					}
					catch (Exception e_)
					{
						Logger.log(Log.WARN, handler_, e_);
					}
				}
			}

			@Override
			public void cancel()
			{
				manager.cancel(intent);
				context.unregisterReceiver(this);
			}
		};

		// schedule handler
		long interval = SystemClock.elapsedRealtime() + interval_;
		handler_.schedule(receiver, interval);
		IntentFilter filter = new IntentFilter(action);
		context.registerReceiver(receiver, filter);
		manager.set(AlarmManager.ELAPSED_REALTIME, interval, intent);
	}

	/**
	 * perform action with delay. Don't prevent CPU from sleeping. If given action was already
	 * scheduled, then previous handler will be cancelled. Action will be executed on main thread.
	 *
	 * @param delay_   given delay. Deep sleep time will added to the interval.
	 * @param handler_ handler that will handle the action.
	 */
	public static void postponeAction(long delay_, @NonNull final TimeHandler handler_)
	{
		// if handler already scheduled
		if (handler_.getTimeout() >= 0)
		{
			// cancel handler
			handler_.cancel();
		}

		// schedule handler
		CancellableRunnable task = new CancellableRunnable()
		{
			public void run()
			{
				try
				{
					// if handler was not cancelled or restarted
					if (handler_.task == this)
					{
						handler_.callback();
					}
				}
				catch (Exception e_)
				{
					Logger.log(Log.WARN, handler_, e_);
				}
			}

			@Override
			public void cancel()
			{
				sHandler.removeCallbacks(this);
			}
		};

		// schedule handler
		handler_.schedule(task, SystemClock.elapsedRealtime() + delay_);
		assert handler_.task != null;
		sHandler.postDelayed((Runnable)handler_.task, delay_);
	}

	private static void keepTime()
	{
		// compute how much time passed
		long now = SystemClock.elapsedRealtime();

		// update time
		sState.realTime += now - sState.elapsedTime;

		// store labels
		sState.elapsedTime = now;
		sState.localTime = System.currentTimeMillis();
		sState.saveState();
	}
}