package borg.framework.services;

import android.util.Log;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Method;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.auxiliaries.Logger;
import borg.framework.resources.Constants;

public final class BinaryReader
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** stream to read **/
	@NonNull
	private final byte[] mStream;

	/** current index in the stream **/
	private int mIndex;

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public BinaryReader(@NonNull byte[] stream_)
	{
		mStream = stream_;
		mIndex = 0;
	}

	@Contract(pure = true)
	public byte readInt8(byte default_)
	{
		return (byte)readInteger(Constants.SIZE_INT8, default_);
	}

	@Contract(pure = true)
	public short readInt16(short default_)
	{
		return (short)readInteger(Constants.SIZE_INT16, default_);
	}

	@Contract(pure = true)
	public int readInt32(int default_)
	{
		return (int)readInteger(Constants.SIZE_INT32, default_);
	}

	@Contract(pure = true)
	public long readInt64(int default_)
	{
		return readInteger(Constants.SIZE_INT64, default_);
	}

	@Contract(pure = true)
	@NonNull
	public <T extends Enum<T>> T readEnum(@NonNull T default_)
	{
		try
		{
			// get values
			Method method = default_.getClass().getMethod("values");
			//noinspection unchecked
			T[] values = (T[])method.invoke(null);
			assert values != null;

			return values[readInt8((byte)default_.ordinal())];
		}
		catch (Exception e)
		{
			Logger.log(Log.WARN, e);
		}

		return default_;
	}

	@Contract(pure = true)
	public byte[] readInt8Array(@Nullable byte[] default_)
	{
		// read size
		int size = readInt16((short)-1);
		if (size > 0)
		{
			byte[] array = new byte[size];
			for (int i = 0; i < size; ++i)
			{
				array[i] = (byte)readInteger(Constants.SIZE_INT8, -1);
			}

			return array;
		}

		return default_;
	}

	@Contract(pure = true)
	private long readInteger(int size_, long default_)
	{
		// if integer can be read
		if (mStream.length >= mIndex + size_)
		{
			long value = 0;
			for (int i = size_ - 1; i >= 0; --i)
			{
				value = (value << 8) + ((mStream[mIndex + i]) & 0xff);
			}

			mIndex += size_;

			return value;
		}

		return default_;
	}
}
