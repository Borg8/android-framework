package borg.framework.services;

import org.jetbrains.annotations.Contract;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import borg.framework.auxiliaries.Event;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.resources.structures.Result;

public final class MissionsManager extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	public static final class Mission
	{
		public enum State
		{
			/** 0: unknown state **/
			UNKNOWN,

			/** 1: mission is active **/
			ACTIVE,

			/** 2: mission is suspended **/
			SUSPENDED,

			/** 3: mission is failed **/
			FAILED,

			/** 4: mission is released **/
			RELEASED
		}

		public interface Executor
		{
			/**
			 * on mission success.
			 *
			 * @param mission_ mission that succeeded.
			 */
			void onSuccess(@NonNull Mission mission_);

			/**
			 * on mission suspend.
			 *
			 * @param mission_ that suspended.
			 */
			void onSuspend(@NonNull Mission mission_);

			/**
			 * on mission fail.
			 *
			 * @param mission_ mission that failed.
			 */
			void onFail(@NonNull Mission mission_);
		}

		/** no suspension on the mission event **/
		public final Event<Mission> running;

		/** mission TAG **/
		@NonNull
		public final String tag;

		/** mission failures **/
		private final LinkedList<Result<? extends Enum>> failures;

		/** mission suspenders **/
		private final HashSet<Object> suspenders;

		/** sign whether mission is released **/
		private boolean isReleased;

		private Mission(@NonNull String tag_)
		{
			running = new Event<>();

			tag = tag_;
			suspenders = new HashSet<>();
			failures = new LinkedList<>();
			isReleased = false;
		}

		/**
		 * @return mission TAG.
		 */
		@Contract(pure = true)
		@NonNull
		public String getTag()
		{
			return tag;
		}

		/**
		 * @return mission state.
		 */
		@Contract(pure = true)
		@NonNull
		public State getState()
		{
			// if mission failed
			if (failures.isEmpty() == false)
			{
				return State.FAILED;
			}

			// if mission suspended
			if (suspenders.isEmpty() == false)
			{
				return State.SUSPENDED;
			}

			// if mission released
			if (isReleased == true)
			{
				return State.RELEASED;
			}

			// mission active
			return State.ACTIVE;
		}

		/**
		 * poll last failure.
		 *
		 * @return popped failure or null if no failure remains.
		 */
		public Result<? extends Enum> pollFailure()
		{
			return failures.pollLast();
		}

		/**
		 * execute mission. After successful execution or failure mission will be dismissed.
		 *
		 * @param executor_ mission executor.
		 */
		public void execute(@NonNull final Executor executor_)
		{
			switch (getState())
			{
				case ACTIVE:
					removeMission(tag);
					executor_.onSuccess(this);
					break;

				case FAILED:
					removeMission(tag);
					executor_.onFail(this);
					break;

				case SUSPENDED:
					executor_.onSuspend(this);

					// if mission is not released
					if (isReleased == false)
					{
						running.attachOnce(new Event.Observer<Mission>(this)
						{
							@Override
							public void action(Mission param_)
							{
								execute(executor_);
							}
						});
					}
					break;

				default:
					break;
			}
		}

		private void suspend(@NonNull Object suspender_)
		{
			suspenders.add(suspender_);
		}

		private void unsuspend(@NonNull Object suspender_)
		{
			// remove suspender
			if (suspenders.remove(suspender_) == true)
			{
				// if last suspender removed
				if (suspenders.isEmpty() == true)
				{
					running.invoke(this);
				}
			}
		}

		private void fail(@NonNull Result<? extends Enum> cause_)
		{
			failures.addLast(cause_);
		}

		/**
		 * release mission.
		 */
		public void release()
		{
			running.detachAll();
			isReleased = true;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/** all missions. Map from TAG to mission **/
	private static final HashMap<String, Mission> sMissions = new HashMap<>();

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private MissionsManager()
	{
		// private constructor to prevent instantiation
	}

	/**
	 * @return active mission or {@code null} if the mission is not exists.
	 */
	@Nullable
	@Contract(pure = true)
	public static Mission getMission(String tag_)
	{
		return sMissions.get(tag_);
	}

	/**
	 * add new mission. If mission with given TAG already exists then no mission will be added.
	 *
	 * @param tag_ TAG to add mission with.
	 *
	 * @return mission annotated to given TAG.
	 */
	@NonNull
	public static Mission addMission(@NonNull String tag_)
	{
		// if mission is not exists or finished
		Mission mission = getMission(tag_);
		if ((mission == null) || (mission.getState() == Mission.State.RELEASED))
		{
			mission = new Mission(tag_);
			sMissions.put(tag_, mission);
		}

		return mission;
	}

	/**
	 * remove and release active mission.
	 *
	 * @param tag_ TAG to remove mission for.
	 */
	public static void removeMission(@NonNull String tag_)
	{
		// remove mission
		Mission mission = sMissions.remove(tag_);

		// if mission found
		if (mission != null)
		{
			mission.release();
		}
	}

	/**
	 * suspend active mission. Suspender may suspend only one mission simultaneously.
	 *
	 * @param tag_           TAG to suspend mission for.
	 * @param timeout_       suspension timeout.
	 * @param suspender_     the suspender.
	 * @param expiredResult_ result on suspension expiration. If {@code null}, then expiration not set.
	 */
	public static void suspendMission(@NonNull final String tag_,
		long timeout_,
		@NonNull final Object suspender_,
		@Nullable final Result<? extends Enum> expiredResult_)
	{
		final Mission mission = sMissions.get(tag_);

		// if mission found
		if (mission != null)
		{
			// suspend active mission
			mission.suspend(suspender_);

			// if expiration was set
			if (expiredResult_ != null)
			{
				TimeManager.postponeAction(timeout_, new TimeManager.TimeHandler()
				{
					@Override
					public void onCallback()
					{
						// if mission is still suspended
						if (mission.getState() == Mission.State.SUSPENDED)
						{
							failMission(mission, suspender_, expiredResult_, true);
						}
					}
				});
			}
		}
	}

	/**
	 * release mission suspension.
	 *
	 * @param tag_       TAG to unsuspend mission for.
	 * @param suspender_ the suspender.
	 */
	public static void unsuspendMission(@NonNull String tag_, @NonNull Object suspender_)
	{
		Mission mission = sMissions.get(tag_);

		// if mission found
		if (mission != null)
		{
			mission.unsuspend(suspender_);
		}
	}

	/**
	 * fail mission.
	 *
	 * @param tag_       TAG to fail mission for.
	 * @param actor_     actor that fail the mission.
	 * @param cause_     cause of failure.
	 * @param suspended_ if false then only if mission is active it will be failed, if true then
	 *                   also suspended mission will be failed.
	 */
	public static void failMission(@NonNull String tag_,
		@NonNull Object actor_,
		@NonNull Result<? extends Enum<?>> cause_,
		boolean suspended_)
	{
		Mission mission = sMissions.get(tag_);

		// if mission found
		if (mission != null)
		{
			failMission(mission, actor_, cause_, suspended_);
		}
	}

	private static void failMission(@NonNull Mission mission_,
		@NonNull Object actor_,
		@NonNull Result<? extends Enum> cause_,
		boolean suspended_)
	{
		Mission.State state = mission_.getState();

		// if mission is active
		if (state == Mission.State.ACTIVE)
		{
			// fail mission
			mission_.fail(cause_);
		}
		else
		{
			// if mission is suspended and shall be failed
			if ((state == Mission.State.SUSPENDED) && (suspended_ == true))
			{
				// fail mission
				mission_.fail(cause_);

				// unsuspend mission
				mission_.unsuspend(actor_);
			}
		}
	}
}
