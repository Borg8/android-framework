package borg.framework.services;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Process;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.Contract;

import java.io.File;
import java.util.ArrayList;

import borg.framework.auxiliaries.Logger;
import borg.framework.holders.GlobalsHolder;

@TargetApi(23)
public final class PermissionsManager
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	private PermissionsManager()
	{
		// private constructor to avoid instantiation
	}

	/**
	 * get accessibility service enable state.
	 *
	 * @return {@code true} if accessibility service is enabled for the package, false otherwise.
	 */
	@Contract(pure = true)
	public static boolean isAccessServiceEnabled()
	{
		try
		{
			Context context = GlobalsHolder.getContext();
			ContentResolver resolver = context.getContentResolver();

			// if service is enabled
			int state = Settings.Secure.getInt(resolver, Settings.Secure.ACCESSIBILITY_ENABLED);
			if (state == 1)
			{
				// get list of all enabled services
				String list = Settings.Secure.getString(resolver,
					Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);

				// if list contains the application package
				return list.contains(context.getPackageName() + File.separatorChar);
			}
		}
		catch (Exception e)
		{
			Logger.log(e); // TODO what if exception occurred every time?
		}

		return false;
	}

	/**
	 * get list of all requested permissions.
	 *
	 * @param context_ application context.
	 *
	 * @return list of all requested permission.
	 */
	@Nullable
	@Contract(pure = true)
	public static String[] getPermissions(@NonNull Context context_)
	{
		PackageManager manager = context_.getPackageManager();

		try
		{
			// get package info
			PackageInfo info = manager.getPackageInfo(context_.getPackageName(),
				PackageManager.GET_PERMISSIONS);
			return info.requestedPermissions;
		}
		catch (Exception e)
		{
			Logger.log(e);
		}

		return null;
	}

	/**
	 * get list of granted or not granted permission.
	 *
	 * @param context_ application context.
	 * @param state_   {@link PackageManager#PERMISSION_GRANTED} or {@link PackageManager#PERMISSION_DENIED}.
	 *
	 * @return list of permissions.
	 */
	@Nullable
	@Contract(pure = true)
	public static String[] getPermissions(@NonNull Context context_, int state_)
	{
		// get list of all permissions
		String[] allPermissions = getPermissions(context_);

		if (allPermissions != null)
		{
			// prepare package date
			int pid = Process.myPid();
			int uid = Process.myUid();

			ArrayList<String> permissions = new ArrayList<>();
			for (String permission : allPermissions)
			{
				// if permission state match to requested
				if (state_ == context_.checkPermission(permission, pid, uid))
				{
					permissions.add(permission);
				}
			}

			return permissions.toArray(new String[0]);
		}

		return null;
	}
}
