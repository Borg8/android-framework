package borg.framework.services;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.Settings;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import androidx.annotation.NonNull;
import borg.framework.auxiliaries.ServiceObject;
import borg.framework.holders.GlobalsHolder;

public final class ApplicationsManager extends ServiceObject
{
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Public Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Constants
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Fields
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	//////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * get activities received the intent.
	 *
	 * @param intent_ intent to get activities for.
	 *
	 * @return list of activities received the intent.
	 */
	@NonNull
	@Contract(pure = true)
	public static List<ResolveInfo> getActivities(@NonNull Intent intent_)
	{
		// get applications
		PackageManager manager = GlobalsHolder.getContext().getPackageManager();
		return manager.queryIntentActivities(intent_, PackageManager.MATCH_DEFAULT_ONLY);
	}

	/**
	 * get application info.
	 *
	 * @param package_ package of the application which info is required.
	 * @param flags_   additional option flags to modify the data returned
	 *
	 * @return required application info.
	 */
	@Nullable
	@Contract(pure = true)
	public static ApplicationInfo getAppInfo(@NonNull String package_, int flags_)
	{
		PackageManager manager = GlobalsHolder.getContext().getPackageManager();
		try
		{
			return manager.getApplicationInfo(package_, flags_);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * get package info.
	 *
	 * @param package_ package which info is required.
	 * @param flags_   additional option flags to modify the data returned
	 *
	 * @return required application info.
	 */
	@Nullable
	@Contract(pure = true)
	public static PackageInfo getPackageInfo(@NonNull String package_, int flags_)
	{
		PackageManager manager = GlobalsHolder.getContext().getPackageManager();
		try
		{
			return manager.getPackageInfo(package_, flags_);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * open application settings.
	 *
	 * @param package_ package of the application which settings to open.
	 */
	public static void openApplicationSettings(@NonNull String package_)
	{
		// show application settings
		Context context = GlobalsHolder.getContext();
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setData(Uri.fromParts("package", package_, null));
		context.startActivity(intent);
	}

	/**
	 * get launch intent for package.
	 *
	 * @param package_ package to get launch intent for.
	 *
	 * @return launch intent if found.
	 */
	@Nullable
	@Contract(pure = true)
	public static Intent getIntentForPackage(@NonNull String package_)
	{
		return GlobalsHolder.getContext().getPackageManager().getLaunchIntentForPackage(package_);
	}

	private ApplicationsManager()
	{
		// private constructor to prevent instantiation
	}
}
